import "styles/app.scss";
import "styles/global.scss";
import { Provider, useSelector } from "react-redux";
import store from "../src/redux/store";
import { Link } from "react-router-dom";
import VideoContainerWrapper from "./components/videoCalling/VideoContainerWrapper";
import { PersistGate } from "redux-persist/integration/react";
import { persistor ,history } from "./redux/store";
import Rating from '../src/components/userRatings/index';
import Home from "pages";


function App(): JSX.Element {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <div className="App">
          <Home history={history} />
          <VideoContainerWrapper />
        </div>
      </PersistGate>
      <Rating />
    </Provider>
  );
}
export {  history, Link }
export default App;
