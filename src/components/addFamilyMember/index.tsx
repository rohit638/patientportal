import { useEffect, useState } from "react";
import {
  Form,
  Input,
  DatePicker,
  Select,
  Modal,
  notification,
  InputNumber,
  Button,
  Spin
} from "antd";
import _ from "lodash";
import SearchLocationInput from "../searchAddress/index";
import { DELETE, GET, POST, PUT } from "../../services/common.api";
import {
  relationShip,
  province,
  doctorNames,
  patientGender,
} from "../../constants/patientProfile.constant";
import moment from "moment";
import { useSelector } from "react-redux";
import axios from "axios";
import PhoneInput from "react-phone-input-2";
import { Height } from "@material-ui/icons";

const AddFamilyMember = ({
  open,
  close,
  title,
  editFamilyMember,
  getFamilyMember,
}) => {
  let user = JSON.parse(localStorage.getItem("user"));
  const { Option } = Select;
  const [, setLat] = useState(null);
  const [, setLng] = useState(null);
  const [address, setAddress] = useState(null);
  const [dateOfBirth, setDateOfBirth] = useState(undefined);
  const [form] = Form.useForm();
  const [familyMemberDetails, setFamilyMemberDetails] = useState({} as any);
  const [btnLoader, setBtnLoader] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState("");
  const [relationship, setRelationship] = useState({} as any);

  const [loader, setLoader] = useState(false);
  const currentAppointmentInfo = useSelector((state: any) => state.curentAppointment);

  const getPatientDetails = async () => {
    try {
      const data = await GET(`user/profile/${editFamilyMember?.familymemberid}`);
      console.log(data.data[0][0], "Family data");
      setFamilyMemberDetails(data.data[0][0])
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    console.log(editFamilyMember)
    if (title == "Add Family Member") {
      form.resetFields();
    }
    if (!_.isEmpty(editFamilyMember)) {
      getPatientDetails()
      form.setFieldsValue(editFamilyMember);
      form.setFieldsValue({
        Email: familyMemberDetails?.email,
        FirstName: familyMemberDetails?.firstname,
        LastName: familyMemberDetails?.lastname,
      });
      setAddress(editFamilyMember.address);
      console.log("editFamilyMember.dateOfBirth", editFamilyMember.dateOfBirth);
      setDateOfBirth(editFamilyMember.dateOfBirth);
    }
  }, [editFamilyMember]);
  useEffect(() => {
    setDateOfBirth(undefined);
  }, []);
  useEffect(() => {
    form.setFieldsValue({
      Email: familyMemberDetails?.email,
      FirstName: familyMemberDetails?.firstname,
      LastName: familyMemberDetails?.lastname,
    });
  }, [familyMemberDetails]);


  const onFinish = async (values) => {
    if (address == "" || address == null) {
      notification.warning({
        message: 'Please Enter your Address !',
      })
    } else {

      setLoader(true);
      console.log(values)
      setBtnLoader(true)
      if (values.ohipNumber != null && values.insuranceServiceNumber != null && values.insuranceProviderName != null) {
        // alert("check valid");
        let data = {
          // 'Provider-number': formValues.Insurancesvcnum,
          'Provider-number': '020497',
          'HCN': values.insuranceServiceNumber,
          'VC': values.insuranceVersionCode,
          'User': values.FirstName + values.LastName
        }
        await axios.post('https://api.mdmax.ca/api/1.1/wf/api-validation-call', data, { headers: { "Authorization": `Bearer 894da2b4b1760319ae94cbfa73db5a10` } })
          .then(async (response: any) => {
            setLoader(false);
            console.log(response, "responce");
            if (response?.data?.response[("MOH-card-eligible")] == false &&
              response?.data?.response[("First-name")]?.toLowerCase() != values?.FirstName.toLowerCase() &&
              response?.data?.response[("Last-name")]?.toLowerCase() != values?.LastName.toLowerCase()
            ) {
              notification.warning({
                message: 'Invalid Insurance Card',
              })
            } else {
              const data = {
                "Type": "PATIENT",
                "FirstName": values.FirstName,
                "LastName": values.LastName,
                "Email": values.Email,
                "phoneNumber": phoneNumber,
                "createDate": moment().utc(),
                // "cognitoid": user.userSub,
                "gender": values.gender
              }
              console.log(data, "dataaaaaaaa")
              const response = await POST('user/patient', data);
              console.log(response);
              let familyAddObject = {
                userName: `${values.FirstName} ${values.LastName}`,
                relationship: values.relationship,
                province: values.province,
                ohipNumber: values.ohipNumber,
                insuranceServiceNumber: values.insuranceServiceNumber,
                insuranceProviderName: values.insuranceProviderName,
                gender: values.gender,
                familyPhysician: values.familyPhysician,
                address,
                dateOfBirth,
                userid: user?.id,
                familymemberid: response?.data?.body?.user?.id,
                active: 1,
                createdBy: user.role,
                id: undefined
              };
              console.log(familyAddObject, "familyAddObject")
              Addfamily(familyAddObject)
            }
          }, (err) => {
            setLoader(false);
            notification.warning({
              message: "Missing parameter for Validation",
            })
          });

      } else {

        const data = {
          "Type": "PATIENT",
          "FirstName": values.FirstName,
          "LastName": values.LastName,
          "Email": values.Email,
          "phoneNumber": phoneNumber,
          "createDate": moment().utc(),
          // "cognitoid": user.userSub,
          "gender": values.gender
        }
        console.log(data, "dataaaaaaaa")
        const response = await POST('user/patient', data);
        console.log(response);
        let familyAddObject = {
          userName: `${values.FirstName} ${values.LastName}`,
          relationship: values.relationship,
          province: values.province,
          ohipNumber: values.ohipNumber,
          insuranceServiceNumber: values.insuranceServiceNumber,
          insuranceProviderName: values.insuranceProviderName,
          gender: values.gender,
          familyPhysician: values.familyPhysician,
          address,
          dateOfBirth,
          userid: user?.id,
          familymemberid: response?.data?.body?.user?.id,
          active: 1,
          createdBy: user.role,
          id: undefined
        };
        console.log(familyAddObject, "familyAddObject")
        Addfamily(familyAddObject)
      }
    }

  };

  const Addfamily = async (familyAddObject: any) => {
    setLoader(false);
    try {
      if (_.isEmpty(editFamilyMember)) {
        await POST("userfamilymembers", familyAddObject);
        setDateOfBirth(undefined);
        notification.success({
          message: "Your Data Successfully Added",
        });
        setBtnLoader(false)
      } else {
        const { id } = editFamilyMember;
        // alert(id)
        familyAddObject.id = id;
        await PUT("userfamilymembers", familyAddObject);
        setDateOfBirth(undefined);
        notification.success({
          message: "Details updated successfully ",
        });
        setBtnLoader(false)
      }
      form.resetFields();
      setDateOfBirth(undefined);
      getFamilyMember();
      close();
    } catch (err) {
      setBtnLoader(false)
      close();
      console.log("error", err);
    }
  }

  const onChangeDateOfBirth = (date, dateString) => {
    setDateOfBirth(dateString);
  };

  return (
    <>
      <Modal
        title={title}
        visible={open}
        footer={null}
        onOk={close}
        onCancel={close}
      >
        <Form layout="vertical" name="basic" form={form} onFinish={onFinish}
          initialValues={{
            birth_date: dateOfBirth == "" ? null : moment(dateOfBirth)
          }}
        >
          <div className="row">
            <div className="col-sm-6">
              <Form.Item
                name="FirstName"
                label="First Name"
                rules={[
                  {
                    required: true,
                    message: "Please Enter First Name",
                  },
                ]}
              >
                <Input placeholder="First Name" name="FirstName" />
              </Form.Item>
            </div>
            <div className="col-sm-6">
              <Form.Item
                name="LastName"
                label="Last Name"
                rules={[
                  {
                    required: true,
                    message: "Please Enter Last Name",
                  },
                ]}
              >
                <Input placeholder="Last Name" name="LastName" />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <Form.Item label="Date Of Birth" name="birth_date"
                rules={[
                  {
                    required: true,
                    message: "Please Enter Date Of Birth",
                  },
                ]}
              >
                <DatePicker
                  style={{ width: "100%" }}
                  name="birth_date"
                  onChange={onChangeDateOfBirth}
                  value={
                    dateOfBirth !== undefined ? moment(dateOfBirth) : undefined
                  }
                  disabledDate={(current) => {
                    return moment().add(-1, "days") <= current;
                  }}
                // defaultValue={moment(dateOfBirth, 'YYYY-MM-DD')}
                />
              </Form.Item>
            </div>
            <div className="col-sm-6">
              <Form.Item
                name="province"
                label="Choose Province"
                rules={[
                  {
                    required: false,
                    message: "Please Enter Company Province",
                  },
                ]}
              >
                <Select
                  showSearch
                  placeholder="Province"
                  optionFilterProp="children"
                >
                  {province.map((provinceData) => (
                    <Option value={provinceData.id}>{provinceData.name}</Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <Form.Item
                name="Email"
                label="Email"
                rules={[
                  {
                    required: true,
                    message: "Please Enter Email",
                  },
                ]}
              >
                <Input placeholder="Email" name="Email" />
              </Form.Item>
            </div>
            <div className="col-sm-6">
              <Form.Item name="familyPhysician" label="Family Physicians">
                <Select
                  showSearch
                  placeholder="Family Physicians"
                  optionFilterProp="children"
                >
                  {doctorNames.map((doctorData) => (
                    <Option value={doctorData.id}>{doctorData.name}</Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <Form.Item
                name="relationship"
                label="Choose Relationship"
                rules={[
                  { required: true, message: "Please Enter Relationship" },
                ]}
              >
                <Select
                  showSearch
                  placeholder="RelationShip"
                  optionFilterProp="children"
                  onChange={(e) => {
                    console.log(e);
                    setRelationship(e)
                  }}
                >
                  {relationShip.map((relation) => (
                    <Option value={relation.id} key={relation.id}>
                      {relation.name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
            <div className="col-sm-6">

              <Form.Item
                name="gender"
                label="Gender"
                rules={[
                  { required: true, message: "Please Enter Gender" },
                ]}
              >
                <Select
                  showSearch
                  placeholder="Select Gener"
                  optionFilterProp="children"
                >
                  {/* {patientGender.map((genderData) => (
                    <Option value={genderData.id}>{genderData.name}</Option>
                  ))} */}

                    <Option value="male">male</Option>
                    <Option value="female">female</Option>
                    <Option value="intersex">intersex</Option>

                  {
                    // relationship == "brother" || relationship == "cousin" || relationship == "father" ||
                    //   relationship == "grandfather" || relationship == "husband" || relationship == "nephew" ||
                    //   relationship == "uncle" || relationship == "son" ?
                    //   <>
                    //     <Option value="male">male</Option>
                    //     <Option value="intersex">intersex</Option>
                    //   </>
                    //   :
                    //   <>
                    //     {/* <Option value="male">male</Option> */}
                    //     <Option value="male">male</Option>
                    //     <Option value="female">female</Option>
                    //     <Option value="intersex">intersex</Option>
                    //   </>
                  }




                </Select>
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <Form.Item
                name="insuranceServiceNumber"
                label="Insurance Service Number"
                rules={[
                  {
                    required: false,
                    message: "Please Enter Insurance Service Number",
                  },
                  {
                    min: 10,
                    message: "must be minimum 10 characters.",
                  },
                ]}
              >
                <Input
                  placeholder="Insurance Service Number"
                  name="insuranceServiceNumber"
                  maxLength={10}
                />
              </Form.Item>
            </div>
            <div className="col-sm-6">
              <Form.Item
                name="ohipNumber"
                label="Ohip Number"
                rules={[
                  { required: false, message: "Please Enter Ohip Number" },
                  {
                    min: 10,
                    message: "Username must be minimum 10 characters.",
                  },
                ]}
              >
                <Input
                  placeholder="Ohip Number"
                  name="ohipNumber"
                  maxLength={10}
                />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <Form.Item
                name="insuranceProviderName"
                label="Insurance Provider Name"
              >
                <Input
                  placeholder="Insurance Provider Name"
                  name="insuranceProviderName"
                />
              </Form.Item>
            </div>
            <div className="col-sm-6">
              <Form.Item
                name="insuranceVersionCode"
                label="Insurance Version Code"
                rules={[
                  {
                    required: false,
                    message: "Please Enter Insurance Version Code",
                  },
                ]}
              >
                <Input
                  placeholder="Insurance Version Code"
                  name="insuranceVersionCode"
                  maxLength={10}
                />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <Form.Item
                name="phoneNumber"
                // label="Phone"
                rules={[
                  {
                    required: false,
                    message: "Please Enter phone number",
                  },
                ]}
              >
                <p className="p-0 mb-1">Phone</p>

                {/* <Input placeholder="Phone" name="Phone" /> */}
                <PhoneInput containerStyle={{ height: 10 }} enableSearch country={'es'} onChange={phone => setPhoneNumber(phone)} />
              </Form.Item>
            </div>
            <div className="col-sm-6">
              <label> <span className="text-danger">*</span> Address</label>
              <Form.Item>
                <SearchLocationInput
                  styleProps={{ minHeight: 30 }}
                  name="address"
                  address={address}
                  setlat={(e) => setLat(e)}
                  setlng={(e) => setLng(e)}
                  setAddress={(e) => setAddress(e)}
                  onBlur={(e) => setAddress(e)}
                />
              </Form.Item>
            </div>
          </div>
          <div
            className="row ml-1 mr-1 border-top"
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <div className="pt-4 pr-3">
              <Form.Item>
                <Button
                  style={{ width: 155, height: 'auto' }}
                  htmlType="submit"
                  className="btn btn-primary px-5"
                  loading={btnLoader}
                >
                  {/* {_.isEmpty(editFamilyMember) ? "Submit" : "Edit"} */}
                  Submit
                </Button>
              </Form.Item>
            </div>
            <div className="pt-4 pr-3">
              <Form.Item>
                <Button
                  style={{ width: 155, height: 'auto' }}
                  htmlType="button"
                  onClick={close}
                  className="btn btn-light px-5"
                >
                  Cancel
                </Button>
              </Form.Item>
            </div>
          </div>
        </Form>
      </Modal>
    </>
  );
};

export default AddFamilyMember;
