// import React from 'react';
import { styled } from '@material-ui/core';
import { DatePicker, Checkbox } from 'antd';
import './style.css'

export const SearchFilter = () => {

    return (

        // <div className="pt-4 pl-4 pr-4 pb-0">
        <div className="pt-4  pb-0">
            <div className="card">
                <div className="card-header headerInfo">
                    <h5>Search Filter</h5>
                </div>
                <div className="card-body">
                    <div className="filter-widget">
                        <div>
                            <DatePicker className="datePicker" />
                        </div>
                    </div>
                    <div className="filter-widget">
                        <h5 className="pt-3">Gender</h5>
                        <div>
                            <Checkbox className="pr-2" >
                                <span className="Info">Male Doctor</span> </Checkbox>
                        </div>
                        <div>
                            <Checkbox className="pr-2" >
                                <span className="Info">Female Doctor</span></Checkbox>

                        </div>
                    </div>
                    <div className="filter-widget">
                        <h5 className="pt-3">Select Specialist</h5>
                        <div>
                            <Checkbox className="pr-2">
                                <span className="Info">Urology</span></Checkbox>

                        </div>
                        <div>
                            <Checkbox className="pr-2">
                                <span className="Info">Neurology</span></Checkbox>

                        </div>
                        <div>
                            <Checkbox className="pr-2">
                                <span className="Info">Dentist</span></Checkbox>

                        </div>
                        <div>
                            <Checkbox className="pr-2">
                                <span className="Info">Orthopedic</span></Checkbox>

                        </div>
                        <div>
                            <Checkbox className="pr-2">
                                <span className="Info">Cardiologist</span></Checkbox>

                        </div>
                        <div>
                            <Checkbox className="pr-2">
                                <span className="Info">Cardiologist</span></Checkbox>

                        </div>
                    </div>
                    <div className="btn-search pt-3 ">
                        <button type="button" className="btn searchBtn">Search</button>
                    </div>
                </div>
            </div>
        </div>


    );
};


