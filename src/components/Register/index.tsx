import React, { useState } from 'react'
import * as Yup from "yup";
import { Auth } from "aws-amplify";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { useHistory } from 'react-router-dom'
import { notification, Spin } from 'antd';
import { POST } from '../../services/common.api';

import moment from 'moment';
import { useDispatch } from 'react-redux';
import { setProfileComplate } from 'redux/actions/userActions';
import SweetAlert from 'react-bootstrap-sweetalert';

const Register = (pros) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [loader, setLoader] = useState(false);
  const [hideAlert, setHideAlert] = useState(false)

  const registerUser = async (values) => {
    console.log(values);

    try {
      setLoader(true);
      const user = await Auth.signUp({
        'username': values.email,
        'password': values.newPasword,
        'attributes': {
          'email': values.email,
          'custom:userType': '6',
          // 'phone_number': '+34' + phoneNumber,
          'phone_number': '+' + values.phone,
          'name': values.firstname + "" + values.lastname
        }
      })
      console.log(user)
      // .then((userData:any) => {
      //   console.log(userData)
      // });

      console.log(user.userSub, "userrererer")

      if (user.userSub) {
        const data = {
          "Type": "TEST",
          "FirstName": values.firstname,
          "LastName": values.lastname,
          "Email": values.email,
          "Phone": values.phone,
          "createDate": moment().utc(),
          "cognitoid": user.userSub,
          "gender": values.gender
        }
        console.log(data, "dataaaaaaaa")
        const response = await POST('user/patient', data);
        console.log(response)
        dispatch(setProfileComplate("false"));
      }

      notification.success({
        message: 'Registration Successfull',
        duration: 2,
        description:
          "Registration Successfull",
        onClick: () => {
          console.log('Notification Clicked!');
        },
      });
      setLoader(false);
      setHideAlert(true)
    } catch (error) {
      notification.error({
        message: error.message,
        duration: 2,
        // description: error.message,
        onClick: () => {
          console.log('Notification Clicked!');
        },
      });
      setLoader(false);
    }





  };

  function validateEmail(value) {
    let error;
    if (!value) {
      error = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      error = 'Email is not a valid email!';
    }
    return error;
  }

  const handleBack = () => {
    setHideAlert(false);
    history.push("/")
  }
  const onCancel = () => {
    setHideAlert(false)
  }

  const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/



  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={{
          firstname: "",
          lastname: "",
          email: "",
          newPasword: "",
          confirmPassword: "",
          phone: "",
          gender: ""
        }}
        validationSchema={Yup.object().shape({
          firstname: Yup.string().required(
            "Please enter First Name !"
          ),
          lastname: Yup.string().required(
            "Please enter Last Name !"
          ),
          email: Yup.string().required(
            "Please enter Valid Email !"
          ),
          // phone: Yup.string().required(
          //   "Please enter Phone!"
          // ),
          phone: Yup.string()
            .required("required")
            .matches(phoneRegExp, 'Phone number is not valid')
            .min(10, "to short")
            .max(10, "to long"),
          gender: Yup.string().required(
            "Please enter Gender!"
          ),
          newPasword: Yup.string()
            .required("Please Enter password !")
            .matches(
              /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
              "Please choose a stronger password. Try a mix of letters, numbers, and symbols"
            ),
          confirmPassword: Yup.string().oneOf(
            [Yup.ref("newPasword"), null],
            "Passwords must match"
          ),
        })}
        onSubmit={(formData, { setStatus, setSubmitting }) => {
          console.log("formData: ", formData);
          registerUser(formData)
          // setSubmitting(true);

        }}
      >
        {({
          errors,
          status,
          touched,
          isSubmitting,
          setFieldValue,
          values,
          handleChange,
          handleBlur,
        }) => {
          return (
            <>
              {
                hideAlert == true ?
                  <SweetAlert
                    success
                    title="Signup successful!"
                    onConfirm={handleBack}
                    onCancel={onCancel}
                    confirmBtnText="Go Login"
                  >
                    Please check your Email and Validate your Account
                  </SweetAlert> : null
              }

              <Form>
                <div className='row pt-5'>
                  <div className='col-sm-6'>
                    <div className="form-group required">
                      <Field
                        name="firstname"
                        type="text"
                        placeholder="First Name *"
                        autocomplete="off"
                        className={
                          "form-control" +
                          (errors.firstname && touched.firstname
                            ? " is-invalid"
                            : "")
                        }
                      />
                      <ErrorMessage
                        name="firstname"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                  </div>
                  <div className='col-sm-6'>
                    <div className="form-group required">
                      <Field
                        name="lastname"
                        type="text"
                        placeholder="Last Name *"
                        autocomplete="off"
                        className={
                          "form-control" +
                          (errors.firstname && touched.firstname
                            ? " is-invalid"
                            : "")
                        }
                      />
                      <ErrorMessage
                        name="lastname"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                  </div>
                </div>

                <div className="form-group required">
                  <Field
                    name="email"
                    type="text"
                    placeholder="Email *"
                    autocomplete="off"
                    validate={validateEmail}
                    className={
                      "form-control" +
                      (errors.email && touched.email
                        ? " is-invalid"
                        : "")
                    }
                  />
                  <ErrorMessage
                    name="email"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>

                <div className="form-group required">
                  <Field
                    name="newPasword"
                    type="text"
                    autocomplete="off"
                    placeholder="New Pasword *"
                    className={
                      "form-control" +
                      (errors.newPasword && touched.newPasword
                        ? " is-invalid"
                        : "")
                    }
                  />
                  <ErrorMessage
                    name="newPasword"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>

                <div className="form-group required">
                  <Field
                    name="confirmPassword"
                    type="text"
                    autocomplete="off"
                    placeholder="Confirm Password *"
                    className={
                      "form-control" +
                      (errors.confirmPassword &&
                        touched.confirmPassword
                        ? " is-invalid"
                        : "")
                    }
                  />
                  <ErrorMessage
                    name="confirmPassword"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>

                <div className="form-group required">
                  <Field
                    name="phone"
                    type="text"
                    autocomplete="off"
                    placeholder="mobile *"
                    className={
                      "form-control" +
                      (errors.phone &&
                        touched.phone
                        ? " is-invalid"
                        : "")
                    }
                  />


                  <ErrorMessage
                    name="phone"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group required">
                  <Field as="select" name="gender"
                    className={
                      "form-control" +
                      (errors.gender &&
                        touched.gender
                        ? " is-invalid"
                        : "")
                    }
                  >
                    <option> Select gender </option>
                    <option value="male">male</option>
                    <option value="female">female</option>
                    <option value="other">other</option>
                  </Field>
                  <ErrorMessage
                    name="gender"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>



                <div className="form-group text-center mb-0 m-t-20">
                  <div className="offset-md-3 col-md-6 col-xs-12 d-flex justify-content-center w-100">
                    {
                      loader ? <Spin /> :
                        <button
                          type="submit"
                          disabled={isSubmitting}
                          className="btn btn-primary btn-lg btn-block text-uppercase pt-2 "
                          style={{ width: '200px', fontSize: 14, marginTop: '10px' }}
                        >
                          Continue
                        </button>
                    }

                  </div>
                </div>
              </Form>
            </>

          );
        }}
      </Formik>
    </div>
  )
}

export default Register
