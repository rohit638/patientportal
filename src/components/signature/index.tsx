
import React, { Component, useState, useEffect, useRef } from 'react'
import { v4 as uuid } from 'uuid'
import CanvasDraw from 'react-canvas-draw'
import { API } from 'aws-amplify'
import axios from 'axios'
import { CircularProgress } from '@material-ui/core'
import { s3Upload } from '../../services/s3fileUpload/index'

// class OImageCanvas extends Component {
export const OImageCanvas = ({ props }) => {


  const [drawings, setDrawings] = useState([])
  const [saving, setSaving] = useState(false)
  const [key, setKey] = useState(null)
  const [image, setImage] = useState(null)
  const _sketch = useRef(null)

  useEffect(() => {
    setSaving(true);
    setImage(props.image);
    // this.setState({ image: this.props.image, saving: false })
  }, []);


  // const shouldComponentUpdate = (props, state) => {
  //   if (props.userId != this.props.userId || props.image != this.props.image) {
  //     // this.props.done(data.members)
  //     this.setState({ image: props.image, saving: false })

  //     return true
  //   }
  //   return false
  // }

  const _undo = () => {
    // _sketch.undo()
  }

  const _clear = () => {
    // _sketch.clear()
  }

  const _save = async () => {
    // let myID = ''
    // if (props.myID) {
    //   myID = props.myID
    // }
    // const a = document.getElementById(`unique${myID}`)

    // const b = a.childNodes[0]
    // const c = b.childNodes[1]
    // const dataUrl = c.toDataURL()
    // console.log('saved data', dataUrl)
    // setSaving(true);
    // const name = `userSignature/${uuid()}.` + `png`
    // console.log('name::', name)
    // let file : any
    // const arr = dataUrl.split(',')
    // const mime = arr[0].match(/:(.*?);/)[1]
    // const bstr = atob(arr[1])
    // let n = bstr.length
    // const u8arr = new Uint8Array(n)
    // while (n--) {
    //   u8arr[n] = bstr.charCodeAt(n)
    // }
    // file = new Blob([u8arr], { type: mime })
    // console.log('......file', file)
    // // let myfilekey= await formService.uploadFileIntoS3(file,name);

    // // this.props.done(name)

    // const data = {
    //   userId: props.userId,
    //   type: mime,
    //   fileName: name,
    //   formId: props.formId,
    // }
    // // if (props.setLoading) {
    // //   props.setLoading(true)
    // // }
    // console.log('data: ', data)
    // try {
    //   const uplodadedImageKey = await s3Upload(data.fileName, file)
    //   setKey(uplodadedImageKey)
    // } catch (err) {
    //   console.log('err: ', err)
    // }
  }

  const edit = () => {
    // if (this.props.image != this.props.initial) {
    // alert('clear')
    API.del('RESOURCELIBRARY', `user/signature`, {
      body: { userId: props.userId, formId: props.formId },
    }).then((res) => {
      console.log('deleted')
    })
    setImage(false);
    // this.props.done('')
    // formService.removeDocumentFromS3(this.props.image);
    // } else {
    // this.setState({ image: false });
    // this.props.done('')
    // }
  }


  // const { controlledValue } = this.state
  return (
    <div>
      <div
        style={{ overflow: 'hidden', width: '250px' }}
        className="ml-0 border border-primary border-1 text-left"
      >
        {(image === '' || image === null || image === false) &&
          props.hideHead != true ? (
          <div
            id={`unique${props.myID ? props.myID : ''}`}
            style={{ overflow: 'hidden', width: '300px' }}
          >
            <CanvasDraw
              width="250px"
              ref={_sketch}
              // ref={(c) => (_sketch = c)}
              brushColor="black"
              brushRadius={2}
              lazyRadius={2}
              canvasWidth={250}
              canvasHeight={150}
            // done={props.done(key)}
            />
          </div>
        ) : (
          <div
            style={{
              overflow: 'hidden',
              width: '250px',
              height: '150px',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            {saving ? (
              <CircularProgress size="small" color="primary" />
            ) : (
              <img src={image || ''} width=" " alt="" />
            )}
          </div>
        )}
      </div>
      {props.hideHead != true && (
        <div style={{ width: '250px' }} className="p-2 sign-bga text-light ">
          {image === '' || image === null || image === false ? (
            <>
              <button
                className="btn btn-primary btn-rounded mr-1"
                onClick={(e) => {
                  e.preventDefault()
                  _undo()
                }}
              >
                <i className="fa fa-undo" />{' '}
              </button>
              {/* <button className="btn btn-primary btn-rounded mr-1" onClick={(e)=>{e.preventDefault();this._redo()}}><i className="fa fa-redo"></i> </button> */}
              <div className="float-right">
                <button
                  className="btn btn-primary btn-rounded mr-1"
                  onClick={(e) => {
                    e.preventDefault()
                    _save()
                  }}
                >
                  {saving ? (
                    <CircularProgress size="small" color="secondary" />
                  ) : (
                    <i className="fa fa-save" />
                  )}
                </button>
              </div>
            </>
          ) : (
            <button
              className="btn btn-primary btn-rounded mr-1"
              onClick={(e) => {
                e.preventDefault()
                edit()
              }}
            >
              <i className="fa fa-edit" />{' '}
            </button>
          )}
        </div>
      )}
    </div>
  )
}


// export default OImageCanvas
