import moment from "moment";
const pdfContentType = "application/pdf";

const base64toBlob = (data) => {
  const bytes = atob(data);
  let { length } = bytes;
  const out = new Uint8Array(length);

  // eslint-disable-next-line no-plusplus
  while (length--) {
    out[length] = bytes.charCodeAt(length);
  }
  return new Blob([out], { type: pdfContentType });
};

const setStartTimeBasedOnCallBackRequest = (callBackType, time) => {
  if (time == "start") {
    console.log("callBackType: ", callBackType);
    if (callBackType == "Morning") {
      return moment().format("YYYY-MM-DD 08:00:00");
    }
    if (callBackType == "Afternoon") {
      return moment().format("YYYY-MM-DD 12:00:00");
    }
    if (callBackType == "Evening") {
      return moment().format("YYYY-MM-DD 17:00:00");
    }
  }

  if (time == "end") {
    console.log("callBackType: ", callBackType);
    if (callBackType == "Morning") {
      return moment().format("YYYY-MM-DD 12:00:00");
    }
    if (callBackType == "Afternoon") {
      return moment().format("YYYY-MM-DD 17:00:00");
    }
    if (callBackType == "Evening") {
      return moment("17:00:00", "HH:mm:ss").add(7, "h").format("YYYY-MM-DD HH:mm:ss");
    }
  }
};

const setStartTimeBasedOnCallBackRequestWithDate = (callBackType, time, date) => {
  if (time == "start") {
    console.log("callBackType: ", callBackType);
    if (callBackType == "Morning") {
      let A = `${moment(date, "dddd, MMM DD, YYYY").format("YYYY-MM-DD")} 08:00 AM`
      return A
    }
    if (callBackType == "Afternoon") {
      let A = `${moment(date, "dddd, MMM DD, YYYY").format("YYYY-MM-DD")} 12:00 PM`
      return A
    }
    if (callBackType == "Evening") {
      let A = `${moment(date, "dddd, MMM DD, YYYY").format("YYYY-MM-DD")} 05:00 PM`
      console.log(A, "A START")
      return A
    }
  }

  if (time == "end") {
    console.log("callBackType: ", callBackType);
    if (callBackType == "Morning") {
      let A = `${moment(date, "dddd, MMM DD, YYYY").format("YYYY-MM-DD")} 12:00 PM`
      return A
    }
    if (callBackType == "Afternoon") {
      let A = `${moment(date, "dddd, MMM DD, YYYY").format("YYYY-MM-DD")} 05:00 PM`
      return A
    }
    if (callBackType == "Evening") {
      let A = `${moment(date, "dddd, MMM DD, YYYY").format("YYYY-MM-DD")} 09:00 AM`
      console.log(A, "A END")
      return A
    }
  }
};

export { pdfContentType, base64toBlob, setStartTimeBasedOnCallBackRequest, setStartTimeBasedOnCallBackRequestWithDate };
