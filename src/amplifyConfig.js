import { Amplify } from 'aws-amplify'

Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: "ca-central-1",
    userPoolId: "ca-central-1_wfazhhnBE",
    identityPoolId: "ca-central-1:d4a8b51d-5318-442d-b22f-a0d9352a18c9",
    userPoolWebClientId: "58k6i4bk0m9j5of3vkt4a4c9ur",
    storage: sessionStorage,
  },
  Storage: {
    region: "ca-central-1",
    bucket: "wellnesswrx-portal",
    identityPoolId: "ca-central-1:d4a8b51d-5318-442d-b22f-a0d9352a18c9",
    customPrefix: { public: '' },
  },
  // API: {
  //   endpoints: [
  //     {
  //       name: 'ONRX_API',
  //       endpoint: config.ONRX_API.apiGateway.URL,
  //       region: config.ONRX_API.apiGateway.REGION,
  //     },
  //     {
  //       name: 'WELLNESS_API',
  //       endpoint: config.apiUrl,
  //       region: config.ONRX_API.apiGateway.REGION,
  //     },
  //   ],
  // },
})
