// import config from '../../config'
const config = {
  apiUrl: "https://mp0nk7eu31.execute-api.ca-central-1.amazonaws.com/dev/"
}
const axios = require('axios')

// axios.defaults.headers.common.Authorization = "Bearer sdjhjdjhdddjhd"
// axios.defaults.headers.common['Referrer-Policy'] = "no-referrer-when-downgrade";
const GET = (path) => {
  return axios.get(`${config.apiUrl}${path}`).catch((error) => {
    if (error.request) {
      // The request was made but no response was received
      throw error
    } else {
      // Something happened in setting up the request that triggered an Error
      throw error
    }
  })
}

const POST = (path, body) => {
  const options = {
    body: {},
  }
  if (body) {
    options.body = body
  }
  return axios.post(`${config.apiUrl}${path}`, options.body).catch((error) => {
    if (error.request) {
      // The request was made but no response was received
      throw error
    } else {
      // Something happened in setting up the request that triggered an Error
      throw error
    }
  })
}

const PUT = (path, body) => {
  const options = {
    body: {},
  }
  if (body) {
    options.body = body
  }
  return axios.put(`${config.apiUrl}${path}`, options.body).catch((error) => {
    if (error.request) {
      // The request was made but no response was received
      throw error
    } else {
      // Something happened in setting up the request that triggered an Error
      throw error
    }
  })
}

const DELETE = (path) => {
  return axios.delete(`${config.apiUrl}${path}`).catch((error) => {
    if (error.request) {
      // The request was made but no response was received
      throw error
    } else {
      // Something happened in setting up the request that triggered an Error
      throw error
    }
  })
}

export { GET, POST, PUT, DELETE }
