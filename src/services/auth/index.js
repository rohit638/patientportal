import { Auth } from "aws-amplify";

export function currentAccount() {
  // const authResponse = store.get('cognitoResponse')
  return Auth.currentAuthenticatedUser()
    .then((response) => {
      console.log("currentAuthenticatedUser response: ", response);
      if (response) {
        const userDetails =
          (response &&
            response.attributes && {
            ...response.attributes,
            id: response.username,
          }) ||
          {};
        // store.set('accessToken', accessToken)
        return userDetails;
      }
      return false;
    })
    .catch((err) => console.log(err));
}


export function logout() {
  try {
    Auth.signOut();
  } catch (error) {
    console.log('error signing out: ', error);
  }
}