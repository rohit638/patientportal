import apiClient from "../axios";

export function initCallHistory(payload) {
  return apiClient.post(`dev/callhistory`, payload).then((response) => {
    if (response) {
      return response.data;
    }
    return {};
  });
}

export function updateCallHistory(payload) {
  return apiClient.put(`dev/callhistory`, payload).then((response) => {
    if (response) {
      return response.data;
    }
    return {};
  });
}
