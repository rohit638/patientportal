import apiClient from "../axios";

export function getUser(option) {
  let query;
  if (option.id) {
    query = `?id=${option.id}`;
  } else if (option.cognitoid) {
    query = `?cognitoid=${option.cognitoid}`;
  } else {
    query = null;
  }
  return apiClient
    .get(`/dev/user${query}`)
    .then((response) => {
      if (response) {
        return response.data;
      }
      return false;
    })
    .catch((err) => console.log(err));
}
