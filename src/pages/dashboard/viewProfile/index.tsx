import { TopBar } from "components";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEquals, faMapMarked, faMapMarkedAlt, faMapPin, faMapSigns, faMarker } from '@fortawesome/free-solid-svg-icons'
import DashboardSidebar from '../sidebar/sidebar';
import style from './style.module.scss'
import { useEffect, useState } from "react";
import IMG01 from "../../../images/user.png";
import { PUT, GET, POST } from "../../../services/common.api";
import Footer from "components/footerFix";
import { Badge, Button, Cascader, Drawer, Popconfirm, Rate, Space, Table, Tabs, Tooltip, Typography } from "antd";
import { useSelector } from "react-redux";
import moment from "moment";
import { useHistory, Link } from "react-router-dom"
import { PanoramaFishEyeOutlined } from "@material-ui/icons";

import DatePicker from "react-datepicker";

const ViewProfile = () => {
    const history = useHistory();
    const { Text } = Typography
    const [familyDoc, setFamilyDoc] = useState({} as any);
    const [insuranceData, setInsuranceData] = useState([] as any);

    const userData = useSelector((state: any) => state.userReducer.user);
    let config = useSelector((state: any) => state.AppConfig.config);
    const getSelectdFamilyDoctor = async () => {
        try {
            const familyDoctor = await GET(`patient/familydoc/${userData.id}`);
            console.log(familyDoctor.data.body, "Family Doctor data");
            setFamilyDoc(familyDoctor?.data?.body?.CompanyEmployee?.CompanyEmployee);
            console.log(familyDoctor?.data?.body?.CompanyEmployee?.CompanyEmployee.id, "iddididididiididididid")
            // setSelectFamilyMemberID(familyDoctor?.data?.body?.CompanyEmployee?.CompanyEmployee.id);
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        window.scrollTo(0, 0);
        getSelectdFamilyDoctor()
        console.log(userData, "profile view")
        console.log(familyDoc, "family Doctor********");
        getInsuranceInfo()
    }, [])

    const goEditProfile = () => {
        history.push('profile')
    }

    const [sidebar, setSidebar] = useState(false);
    const [selectedDate, SetSelectedDate] = useState(new Date());
    const showSidebar = () => setSidebar(!sidebar);




    // const claimColumns = [
    //     // {
    //     //   title: 'Claim Id',
    //     //   dataIndex: 'claimid',
    //     //   key: 'no',
    //     //   fixed: 'center',
    //     //   width: '50'
    //     // },
    //     {
    //         title: 'Claim date',
    //         dataIndex: 'claimDate',
    //         key: 'no',
    //         fixed: 'center',
    //     },
    //     {
    //         title: 'Consultig Doctor',
    //         dataIndex: 'consultingdoctor',
    //         key: 'no',
    //         fixed: 'center',
    //     },
    //     {
    //         title: 'Cost Amount',
    //         dataIndex: 'costamount',
    //         key: 'no',
    //         fixed: 'center',
    //     },
    //     {
    //         title: 'Paid  Amount',
    //         dataIndex: 'paidamount',
    //         key: 'no',
    //         fixed: 'center',
    //     },
    //     {
    //         title: 'Payment Method',
    //         dataIndex: 'paymentStatus',
    //         key: 'no',
    //         fixed: 'center',
    //     },
    //     {
    //         title: 'Action',
    //         key: 'operation',
    //         // fixed: 'left',
    //         fixed: 'right',
    //         render: () => (
    //             <div>
    //                 <PanoramaFishEyeOutlined className="ml-3 mb-3 font-size-24" />
    //             </div>
    //         ),
    //     },
    // ]


    // const options = [
    //     {
    //         value: 'all',
    //         label: 'all',
    //     },
    // ]

    function onChange(value) {
        console.log(value)
    }

    const dataSource = [
        {
            key: '1',
            Claimdate: '03-jan-2022',
            ConsultigDoctor: 'John Dow',
            CostAmount: '1500',
            PaidAmount: '2500',
            PaymentMethod: 'Pay',

        },
        {
            key: '1',
            Claimdate: '03-jan-2022',
            ConsultigDoctor: 'John Dow',
            CostAmount: '1500',
            PaidAmount: '2500',
            PaymentMethod: 'Pay',

        },
        {
            key: '1',
            Claimdate: '03-jan-2022',
            ConsultigDoctor: 'John Dow',
            CostAmount: '1500',
            PaidAmount: '2500',
            PaymentMethod: 'Pay',

        },
        {
            key: '1',
            Claimdate: '03-jan-2022',
            ConsultigDoctor: 'John Dow',
            CostAmount: '1500',
            PaidAmount: '2500',
            PaymentMethod: 'Pay',

        },
        {
            key: '1',
            Claimdate: '03-jan-2022',
            ConsultigDoctor: 'John Dow',
            CostAmount: '1500',
            PaidAmount: '2500',
            PaymentMethod: 'Pay',

        },
        {
            key: '1',
            Claimdate: '03-jan-2022',
            ConsultigDoctor: 'John Dow',
            CostAmount: '1500',
            PaidAmount: '2500',
            PaymentMethod: 'Pay',

        },

    ];

    const columns = [
        {
            title: 'Claim date',
            dataIndex: 'Claimdate',
            key: 'Claimdate',
        },
        {
            title: 'Consultig Doctor',
            dataIndex: 'ConsultigDoctor',
            key: 'ConsultigDoctor',
        },
        {
            title: 'Cost Amount',
            dataIndex: 'CostAmount',
            key: 'CostAmount',
        },
        {
            title: 'Paid Amount',
            dataIndex: 'PaidAmount',
            key: 'PaidAmount',
        },
        {
            title: 'Payment Method',
            dataIndex: 'PaymentMethod',
            key: 'PaymentMethod',
        },
    ];


    const InsuranceColumns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Type',
            dataIndex: 'insurance_type',
            key: 'insurance_type',
        },
        {
            title: 'Insurance Number',
            dataIndex: 'insurance_number',
            key: 'insurance_number',
        },
        // {
        //     title: 'Version code',
        //     dataIndex: 'address',
        //     key: 'address',
        // },

        // {
        //     title: 'Action',
        //     key: 'action',
        //     render: (text, record) => (
        //         <Space size="middle">
        //             <Tooltip title="Delete" placement="bottom">
        //                 <Popconfirm
        //                     title="Are you sure?"
        //                     onConfirm={confirm}
        //                     onCancel={cancel}
        //                     okText="Yes"
        //                     cancelText="No"
        //                 >
        //                     <Button shape="circle" danger icon={<DeleteFilled />} />
        //                 </Popconfirm>
        //             </Tooltip>
        //         </Space>
        //     ),
        // },
    ];

    const data = [
        {
            key: '1',
            name: '1',
            age: 321234678,
            address: '123456789',
        }
    ];


    const getInsuranceInfo = async () => {
        try {
            const res = await GET(`user/insurance/${userData.id}`,)
            console.log(res?.data, "getInsuranceInfo");
            setInsuranceData(res?.data)
        } catch (err) {
            console.log('error', err);
        }
    }


    const ClickToDayDate = () => {
        // let date = moment().format('YYYY-MM-DD')
        // SetSelectedDate(date);

        let date = new Date()

        SetSelectedDate(date);
    }


    const [visible, setVisible] = useState(false);
    const showDrawer = () => {
        setVisible(true);
    };
    const onClose = () => {
        setVisible(false);
    };

    return (
        <div className="pb-5">
            <TopBar />
            <div className="box">
                <div className={`${style.toggebtn} d-block d-xl-none`}>
                    <i><FontAwesomeIcon className="mr-1" icon={faEquals} onClick={showDrawer} /></i>
                </div>
                <div className="d-block d-xl-none">
                    <p className="pageInfoM">Profile</p>
                    <h5 className="h5NewM">Dashboard / Profile</h5>
                </div>
                <div className="d-none d-xl-block">
                    <p className="pageInfo">Profile</p>
                    <h5 className="h5New"><Link style={{ color: "white" }} to="/dashboard" >Dashboard</Link> / Profile</h5>
                </div>

            </div>
            <div>
                <div className="content" style={{ backgroundColor: '#f1f5f9', paddingTop: 15 }}>
                    <div className={style.container} >
                        <div className="row pb-5 ">
                            <div className={`col-lg-4 col-xl-2 col-sm-12 d-none d-xl-block theiaStickySidebar pr-0`}>
                                <DashboardSidebar />
                            </div>
                            <Drawer closable={false} width={300} placement="left" onClose={onClose} visible={visible} style={{ paddingTop: 0 }}>
                                <div className="widget-profile pro-widget-content pt-0 pb-4">
                                    <div className="profile-info-widget">
                                        <img src="../Images/logo.png" height="50" alt="" />
                                    </div>
                                </div>
                                <DashboardSidebar />
                            </Drawer>

                            {/* {sidebar ?
                                <div className={`d-block d-xl-none ${style.toggle}`}>
                                    <DashboardSidebar />
                                </div> : null
                            } */}
                            <div className="col-lg-12 col-xl-10">
                                <div className="row">
                                    <div className="col-sm-12 col-md-6 col-lg-6 col-xl-5">
                                        <Badge.Ribbon text={`${userData?.role}`}>
                                            <div className="card card-top card-top-primary pb-1 mb-2">
                                                <div className="card-body p-2">
                                                    <div className="media align-items-center">
                                                        <div style={{ position: 'relative', backgroundColor: 'white' }}>
                                                            {/* <img className="mr-3 profpic" src="https://doccure-html.dreamguystech.com/template/assets/img/doctors/doctor-thumb-02.jpg" alt="Mary Stanform" /> */}
                                                            {userData?.profilepicture != "" && userData?.profilepicture != null ? (
                                                                <img className="mr-3 profpic" src={`${process.env.REACT_APP_ASSET_URL}/${userData.profilepicture}`} alt="User" />) : (
                                                                <img className="mr-3 profpic" src={IMG01} alt="User" />)
                                                            }
                                                            {/* <img className="editIcon" style={{ cursor: 'pointer' }} src="Images/edit.png" alt="" onClick={() => goEditProfile()} /> */}
                                                        </div>
                                                        <div className="media-body">
                                                            <div className="pl-2">
                                                                <div className="">
                                                                    <span className="text-uppercase mr-auto fontColor" style={{ margin: 0, fontSize: 15, color: "#134f85" }}>
                                                                        {userData?.FirstName} {userData?.MiddleName} {userData?.LastName}
                                                                    </span>
                                                                </div>
                                                                <div className="font-size-12 mb-1">{userData.Email}</div>
                                                            </div>

                                                            {/* <span>{userData.Email}</span> */}

                                                            <span onClick={() => goEditProfile()} className={`btn btn-primary ml-2 ${style.btnWithAddon}`}>
                                                                {/* <img className="editIcon" style={{ cursor: 'pointer' }} src="Images/edit.png" alt="" /> */}
                                                                Edit Profile
                                                            </span>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Badge.Ribbon>
                                    </div>

                                    <div className="col-sm-12 col-md-6 col-lg-6 col-xl-5">
                                        <div className="card pb-1 mb-3">
                                            <div className="card-body text-white rounded" style={{ backgroundColor: '#ffff' }}>
                                                <div>
                                                    <div className="d-flex mb-1">
                                                        <div className="text-uppercase mr-auto fontColor">Balance</div>
                                                        <div className="fontColor">TOTAL</div>
                                                    </div>
                                                    <div className="d-flex mb-2">
                                                        <div className="font-size-24 font-weight-bold mr-auto fontColor">{config?.currency_symbol} 3,000</div>
                                                        <div className="font-size-24 fontColor"> {config?.currency_symbol} 5,000</div>
                                                    </div>
                                                    <div className="progress">
                                                        <div
                                                            className="progress-bar fontColor"
                                                            style={{
                                                                width: '60%',
                                                                backgroundColor: '#194f85'
                                                            }}
                                                            role="progressbar"
                                                            aria-valuenow={60}
                                                            aria-valuemin={0}
                                                            aria-valuemax={100}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 col-md-0 col-lg-0 col-xl-2" />
                                </div>
                                <div className="card pb-1 mb-3" style={{ paddingBottom: 10 }}>
                                    <div className="card-body m-2">
                                        <div className="row">
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>Name</span>
                                                    <div>
                                                        <strong>{userData?.FirstName} {userData?.MiddleName} {userData?.LastName}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>Address</span>
                                                    <div>
                                                        <strong>{userData.Address1}, {userData.Address2} </strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>Email</span>
                                                    <div>
                                                        <strong> {userData.Email}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>Phone</span>
                                                    <div>
                                                        <strong>{userData.phoneNumber}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>Speciality</span>
                                                    <div>
                                                        <strong>{userData.Speciality}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>Languages</span>
                                                    <div>
                                                        <strong>{userData.Languages || config.language}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>City</span>
                                                    <div>
                                                        <strong>{userData?.City}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>State</span>
                                                    <div>
                                                        <strong>{userData.State}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>Postal Code</span>
                                                    <div>
                                                        <strong>{userData.PostalCode}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>Country</span>
                                                    <div>
                                                        <strong>{userData.Country}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>Department</span>
                                                    <div>
                                                        <strong>{userData.Department}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* <div className="col-md-4 col-sm-6">
                                                <div className="form-group">
                                                    <span>Fax</span>
                                                    <div>
                                                        <strong>1234567890</strong>
                                                    </div>
                                                </div>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-12 col-md-12 p-0 m-0">
                                    <div className={`${style.Cardheader} card card-top card-top-primary pb-1 mb-3`}>
                                        <div className="p-4">
                                            <Tabs defaultActiveKey="1" className={` ${style.tabsW} vb-tabs-bold`}>
                                                <Tabs.TabPane tab="Claim History" key="1">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xl-9 col-lg-8 col-md-8">
                                                                <div className="row">
                                                                    <div
                                                                        className="col-xl-4 col-lg-4 col-md-4 col-sm-12 pr-0 pb-4"
                                                                        style={{
                                                                            width: '100%',
                                                                            display: 'flex',
                                                                            flexDirection: 'row',
                                                                            alignItems: 'center',
                                                                            justifyContent: 'flex-start',
                                                                        }}
                                                                    >
                                                                        <Text className="pr-3">Filters: </Text>

                                                                        <DatePicker dateFormat="MM-dd-yyyy" selected={selectedDate} onChange={(date) => SetSelectedDate(date)} />

                                                                        {/* {
                                                                            selectedDate == "" ?
                                                                                <DatePicker onChange={onChangeDate} style={{ width: '100%' }} /> :
                                                                                <DatePicker onChange={onChangeDate} value={moment(selectedDate, "YYYY-MM-DD")} style={{ width: '100%' }} />

                                                                        } */}

                                                                        {/* <DatePicker onChange={onChangeDate} defaultValue={moment(selectedDate, "YYYY-MM-DD")} style={{ width: '100%' }} /> */}
                                                                    </div>
                                                                    <div className="col-xl-2 col-lg-4 col-md-4 col-sm-12 pr-0 pb-4">
                                                                        <Button onClick={() => ClickToDayDate()}
                                                                            style={{ width: '100%', height: '100%' }}
                                                                        >
                                                                            <Text>Today</Text>
                                                                        </Button>
                                                                    </div>
                                                                    {/* <div className="col-xl-3 col-lg-4 col-md-4 col-sm-12 pr-0 pb-4">
                                                                        <Cascader
                                                                            options={options}
                                                                            onChange={onChange}
                                                                            placeholder="All"
                                                                            style={{ width: '100%' }}
                                                                        />
                                                                    </div> */}
                                                                    <div className="col-xl-2 col-lg-0 col-md-0" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <Table
                                                            // rowKey={(obj) => obj.id}
                                                            // rowSelection={{ type: selectionType, ...rowSelection }}
                                                            className="text-center"
                                                            columns={columns}
                                                            dataSource={dataSource}
                                                            pagination={{ pageSize: 3 }}
                                                        />
                                                    </div>
                                                </Tabs.TabPane>
                                                <Tabs.TabPane tab="Insurance Provider" key="2">
                                                    <Table columns={InsuranceColumns} dataSource={insuranceData} pagination={{ pageSize: 4 }} />
                                                </Tabs.TabPane>
                                            </Tabs>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer" >
                    <Footer />
                </div>
            </div >
        </div >
    );
};

export default ViewProfile;