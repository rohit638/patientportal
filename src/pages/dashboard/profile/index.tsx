import { TopBar } from "components";
import { useHistory, Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEquals, faPlus, faUpload } from "@fortawesome/free-solid-svg-icons";
import DashboardSidebar from "../sidebar/sidebar";
import style from "./style.module.scss";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import moment from "moment";
import { useDispatch } from "react-redux";
import { PUT, GET, POST, DELETE } from "../../../services/common.api";
import { v4 as uuidv4 } from 'uuid';
import { s3Upload } from '../../../services/s3fileUpload/index'
import {
  setUser,
  setProfileComplate,
  setFamilyDoctor,
} from "../../../redux/actions/userActions";
import {
  Form,
  notification,
  Select,
  Spin,
  Modal,
  Space,
  Tag,
  Table,
  Tooltip,
  Button,
  Popconfirm,
  Input,
  Drawer,
  DatePicker,
} from "antd";
import Footer from "components/footerFix";
// import DatePicker from "react-datepicker";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import axios from "axios";
// import "react-datepicker/dist/react-datepicker.css";
import { DeleteFilled } from "@ant-design/icons";
import CountrysJson from "../../../jsonFiles/country.json";
// import { select } from "redux-saga/effects";

const Profile = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const history = useHistory();
  const { Option } = Select;
  const [userInfo, setUserInfo] = useState({} as any);
  const [dob, setDob] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const [countryJson, setCountryJson] = useState({} as any);
  const [selectCountry, setSelectCountry] = useState({} as any);
  const [validPostCode, setValidPostCode] = useState({} as any);

  const [selectedFamilyMemberID, setSelectFamilyMemberID] = useState("");
  const [insuranceType, setInsuranceType] = useState("PRIVATE_INSURANCE");
  const [allDoctor, setAllDoctor] = useState([]);
  const [btnLoader, setBtnLoader] = useState(false);
  const [formValues, setFormValues] = useState({} as any);
  const [loader, setLoader] = useState(false);
  const [imgLoader, setImgLoader] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [insuranceData, setInsuranceData] = useState([] as any);
  let config = useSelector((state: any) => state.AppConfig.config);

  const userData = useSelector((state: any) => state.userReducer.user);
  const familyD = useSelector((state: any) => state?.userReducer?.familyDoc);
  const [ProfilePath, setprofilePath] = useState('')

  const [userProfileKey, setUserProfileKey] = useState(null)

  const [startDate, setStartDate] = useState(null);

  const handleChange = (event) => {
    const name = event?.target?.name;
    const value = event?.target?.value;
    console.log(name, value);
    setFormValues((formValues) => ({ ...formValues, [name]: value }));
    console.log(formValues, "formValues");
  };

  // const handleChangeDob = (event) => {
  //     console.log(moment(event).format('YYYY-MM-DD'))
  //     setDob(moment(event).format('YYYY-MM-DD'))
  // }

  const handleChangeMemeber = (event) => {
    console.log(event);
    setSelectFamilyMemberID(event);
  };

  const getInsuranceInfo = async () => {
    try {
      const res = await GET(`user/insurance/${userData.id}`);
      console.log(res?.data, "getInsuranceInfo");
      setInsuranceData(res?.data);
    } catch (err) {
      console.log("error", err);
    }
  };

  // const getSelectdFamilyDoctor = async () => {
  //     if (userData.id !== "") {
  //         try {
  //             const familyDoctor = await GET(`patient/familydoc/${userData?.id}`);
  //             console.log(familyDoctor.data?.body, "Family Doctor data");
  //             setFamilyDisplayId(familyDoctor?.data?.body?.CompanyEmployee?.CompanyEmployee)
  //             familyDoctor.name = familyDoc.FirstName + familyDoc.MiddleName + familyDoc.LastName
  //             dispatch(setFamilyDoctor(`Dr. ${familyDoctor.data?.body?.CompanyEmployee?.CompanyEmployee?.FirstName} ${familyDoctor.data?.body?.CompanyEmployee?.CompanyEmployee?.LastName}`))
  //             setFamilyDoc(familyDoctor?.data?.body?.CompanyEmployee?.CompanyEmployee);
  //             console.log(familyDoctor?.data?.body?.CompanyEmployee?.CompanyEmployee.ID, "iddididididiididididid")
  //             setSelectFamilyMemberID(familyDoctor?.data?.body?.CompanyEmployee?.ID);
  //         } catch (error) {
  //             console.log(error)
  //         }
  //     }

  // }

  const getUserDetails = async () => {
    try {
      const userInfo = await GET(`user?cognitoid=${userData.cognitoid}`);
      console.log(userInfo, "update user");
      console.log(userInfo.data?.body, "update user");
      // var data = userInfo.data?.body
      // let Doc = "Dr." + familyDoc.FirstName + familyDoc.LastName
      // console.log(data)
      dispatch(setUser(userInfo.data?.body));
      // dispatch(setFamilyDoctor(Doc))
    } catch (error) {
      console.log(error);
    }
  };

  const handleSubmit = (event) => {
    console.log(event, "dataform ANTD");
    try {
      setLoader(true);
      PUT(`user/patient/profile/${userData.cognitoid}`, event).then(() => {
        // notification.success({
        //     message: 'Your Data Successfully Added',
        // })
        setLoader(false);
        // history.push("/view-profile");
        getUserDetails();

        if (selectedFamilyMemberID) {
          const found = allDoctor.find(
            (element) => element.companyemployeeid == selectedFamilyMemberID
          );
          console.log(found, "-*-*-*-**");
          dispatch(
            setFamilyDoctor(`Dr. ${found?.FirstName} ${found?.LastName}`)
          );
        }
        dispatch(setProfileComplate("true"));
        updateOrCreateFamilyMember();
        try {
          PUT(`user/patient/profile/${userData.cognitoid}`, {
            ...formValues,
            DOB: startDate,
            phoneNumber: phoneNumber,
            // profilepicture: userData.profilepicture == "" ? userProfileKey : userData.profilepicture,
            profilepicture: userProfileKey == null ? userData.profilepicture : userProfileKey,
          }).then(() => {
            notification.success({
              message: "Your Data Successfully Added",
            });
            setLoader(false);
            history.push("/view-profile");
            getUserDetails();
            dispatch(setProfileComplate("true"));
          });
        } catch (error) {
          setLoader(false);
          console.log(error);
        }
      });
    } catch (error) {
      setLoader(false);
      console.log(error);
    }
  };

  const updateOrCreateFamilyMember = async () => {
    console.log(selectedFamilyMemberID);
    const familyAddObject = {
      patient_id: userData.id,
      provider_id: selectedFamilyMemberID,
    };
    console.log(familyAddObject);
    try {
      const res = await POST("patient/familydoc", familyAddObject);
      console.log(res, "family doc api ");
    } catch (err) {
      console.log("error", err);
    }
  };

  const handlePostalCode = () => {
    if (config.country == "Canada") {
      setValidPostCode(/^([A-Za-z][0-9][A-Za-z])\s*([0-9][A-Za-z][0-9])$/);
    } else if (config.country == "United States") {
      setValidPostCode(/(^\d{5}$)|(^\d{5}-\d{4}$)/);
    } else if (config.country == "India") {
      setValidPostCode(/^[1-9][0-9]{5}$/);
    }
  };

  useEffect(() => {
    handlePostalCode();
    setCountryJson(CountrysJson);
    console.log(countryJson);
    console.log(userData?.Country, "cut");
    console.log(config?.country, "cut");
    console.log(CountrysJson, "CountrysJson");

    // if(userData?.Country == "" || userData?.Country)
    // let G = userData?.Country == null ? config?.country : userData?.Country;
    let G = config?.country;

    console.log(G, "GGGGGGGGGG");

    var obj = CountrysJson?.countries?.filter((obj) => obj.country === G);
    console.log(obj, "call");
    setSelectCountry(obj[0]);

    setPhoneNumber(userData?.phoneNumber);
    getInsuranceInfo();
    getUserDetails();
    // getSelectdFamilyDoctor();
    if (formValues.DOB !== "") {
      setDob(formValues.DOB);
    }
    if (userData.DOB !== null) {
      setStartDate(
        new Date(moment(userData.DOB, "YYYY-MM-DD").format("MM-DD-YYYY"))
      );
    }

    // if (userData.DOB !== undefined && userData.DOB !== null) {
    //     setFormValues(formValues => ({ ...formValues, DOB: moment(userData.DOB).format('YYYY-MM-DD HH:mm:ss') }));
    // }

    setUserInfo(userData);
    getDoctorList();
  }, []);

  const dateFormat = "YYYY/MM/DD";

  const [sidebar, setSidebar] = useState(false);
  const showSidebar = () => setSidebar(!sidebar);

  const getDoctorList = async () => {
    const doctorList = await GET(`employee/doctors`);
    console.log(doctorList, "-*-*-*All doctorList");
    setAllDoctor(doctorList.data);
  };

  const validateMessages = {
    required: "${label} is required!",
    types: {
      email: "${label} is not a valid email!",
      number: "${label} is not a valid number!",
      code: "${label} is not a valid!",
    },
  };
  /* eslint-enable no-template-curly-in-string */

  const showModal = () => {
    setIsModalVisible(true);
    form.resetFields();
  };

  // const handleOk = async () => {
  // };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  function confirm(e) {
    console.log(e.id);
    try {
      DELETE(`user/insurance/delete/${e?.id}`).then(() => {
        notification.success({
          message: "Successfully Delete",
        });
        getInsuranceInfo();
      });
    } catch (error) {
      console.log(error);
    }
  }
  function cancel(e) {
    console.log(e);
  }

  const columns = [
    // {
    //     title: 'Name',
    //     dataIndex: 'name',
    //     key: 'name',
    //     render: text => <a>{text}</a>,
    // },
    {
      title: "Type",
      dataIndex: "insurance_type",
      key: "insurance_type",
    },
    {
      title: "Insurance Number",
      dataIndex: "insurance_number",
      key: "insurance_number",
    },
    {
      title: "Insurance provider",
      dataIndex: "insurance_provider",
      key: "insurance_provider",
    },

    {
      title: "Action",
      key: "action",
      render: (id) => (
        <Space size="middle">
          <Tooltip title="Delete" placement="bottom">
            <Popconfirm
              title="Are you sure?"
              onConfirm={() => confirm(id)}
              onCancel={cancel}
              okText="Yes"
              cancelText="No"
            >
              <Button shape="circle" danger icon={<DeleteFilled />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  const data = [
    {
      key: "1",
      name: "1",
      age: 321234678,
      address: "123456789",
      type: "Government",
    },
  ];

  function handleChangeSelect(value) {
    console.log(value);
    setInsuranceType(value);
  }

  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  const handleSubmitModal = async (event: any) => {
    console.log("Success:", event);
    setBtnLoader(true);
    if (insuranceType == "OHIP") {
      const result = !!insuranceData.find(
        (item) => item.insurance_number === event.Insurancesvcnum
      );
      const OhipDuplicate = !!insuranceData.find(
        (item) => item.insurance_type === "OHIP"
      );
      if (OhipDuplicate) {
        notification.warning({
          message: "You can add only one OHIP !",
        });
        setBtnLoader(false);
      } else {
        if (result) {
          notification.warning({
            message: "Insurance already added !",
          });
          setBtnLoader(false);
        } else {
          let data = {
            // 'Provider-number': formValues.Insurancesvcnum,
            "Provider-number": "020497",
            HCN: event.Insurancesvcnum,
            VC: event.insuranceVersionCode,
            User: formValues.FirstName || userData.FirstName,
          };
          console.log(data, "check Api  Data");
          setBtnLoader(true);
          await axios
            .post("https://api.mdmax.ca/api/1.1/wf/api-validation-call", data, {
              headers: {
                Authorization: `Bearer 894da2b4b1760319ae94cbfa73db5a10`,
              },
            })
            .then(
              async (response: any) => {
                console.log(response, "responce");
                let D = moment(userData.DOB).format("YYYY-MM-DD");
                if (response?.data?.response["MOH-card-eligible"] == false) {
                  setLoader(false);
                  notification.warning({
                    message: "Invalid Insurance Card",
                  });
                  setBtnLoader(false);
                } else {
                  if (response?.data?.response.DOB !== D) {
                    setLoader(false);
                    notification.warning({
                      message: "Invalid Insurance Card",
                    });
                    setBtnLoader(false);
                  } else {
                    if (
                      response?.data?.response["First-name"].toLowerCase() !==
                      userData.FirstName.toLowerCase()
                    ) {
                      setLoader(false);
                      notification.warning({
                        message: "Invalid Insurance Card",
                      });
                      setBtnLoader(false);
                    } else {
                      if (
                        response?.data?.response["Last-name"].toLowerCase() !==
                        userData.LastName.toLowerCase()
                      ) {
                        setLoader(false);
                        notification.warning({
                          message: "Invalid Insurance Card",
                        });
                        setBtnLoader(false);
                      } else {
                        const Ohip = {
                          name: event.name,
                          number: event?.Insurancesvcnum,
                          provider: "NA",
                          type: insuranceType,
                          vc: event.insuranceVersionCode,
                          user_id: userData.id,
                          is_valid: 1,
                        };
                        console.log(Ohip, "Ohip");
                        try {
                          const res = await POST("user/insurance", Ohip);
                          console.log(res, "family insurances------0 ");
                          notification.success({
                            message: "OHIP details added succesfully",
                          });
                          getInsuranceInfo();
                          setBtnLoader(false);
                          form.resetFields();
                        } catch (err) {
                          console.log("error", err);
                        }
                      }
                    }
                  }
                  setIsModalVisible(false);
                }
              },
              (err) => {
                setBtnLoader(false);
                console.log(err);
              }
            );
        }
      }
    } else {
      setBtnLoader(true);
      console.log(insuranceData);
      console.log(event.Insurancesvcnum);
      const result = !!insuranceData.find(
        (item) => item.insurance_number == event.Insurancesvcnum
      );
      console.log(`resFind(search1): ${result}`);
      if (result) {
        notification.warning({
          message: "Insurance already added !",
        });
        setBtnLoader(false);
      } else {
        const Private = {
          name: event?.name,
          number: event?.Insurancesvcnum,
          provider: event?.provider,
          type: insuranceType,
          vc: "NA",
          user_id: userData.id,
          is_valid: 0,
        };
        console.log(Private, "data Private");

        try {
          setBtnLoader(true);
          const res = await POST("user/insurance", Private);
          console.log(res, "family insurances------0 ");
          notification.success({
            message: "Insurance details added succesfully",
          });
          setBtnLoader(false);
          getInsuranceInfo();
          form.resetFields();
        } catch (err) {
          setBtnLoader(false);
          console.log("error", err);
        }
        setIsModalVisible(false);
        setBtnLoader(false);
      }
    }
  };

  function onChangeCountry(value) {
    console.log(`selected ${value}`);
    console.log(CountrysJson);

    var obj = countryJson?.countries?.filter((obj) => obj.country == value);
    console.log(obj[0]);
    setSelectCountry(obj[0]);
  }

  function onSearchCountry(val) {
    console.log("search:", val);
  }

  function onChangeCity(value) {
    console.log(`selected ${value}`);
  }

  function onSearchCity(val) {
    console.log("search:", val);
  }

  function onDateChange(date, dateString) {
    console.log(date, dateString);
    setStartDate(date);
  }

  const getUserImage = (imageUrl) => {
    if (imageUrl.match(/^http[^\?]*.(jpg|jpeg|gif|png|tiff|bmp)(\?(.*))?$/gim) != null) {
      return imageUrl
    }
    return `${process.env.REACT_APP_ASSET_URL}/${imageUrl}`
  }

  const userProffile = async (event) => {
    const currentFile = event.target.files[0]
    const filename = currentFile?.name
    const fileExtension = filename.substring(filename.lastIndexOf('.'), filename.length)
    const profilePicFileName = `${uuidv4()}${fileExtension}`
    setImgLoader(true)
    const uplodadedImageKey = await s3Upload(profilePicFileName, currentFile)
    setImgLoader(false)
    console.log(uplodadedImageKey, "uplodadedImageKey")
    setUserProfileKey(uplodadedImageKey)
    setprofilePath(URL.createObjectURL(event.target.files[0]))
  }

  return (
    <div className="pb-5">
      <TopBar />
      <div className="box">
        <div className={`${style.toggebtn} d-block d-xl-none`}>
          <i>
            <FontAwesomeIcon
              className="mr-1"
              icon={faEquals}
              onClick={showDrawer}
            />
          </i>
        </div>
        <div className="d-block d-xl-none">
          <p className="pageInfoM"> Profile</p>
          <h5 className="h5NewM">Dashboard / Profile</h5>
        </div>
        <div className="d-none d-xl-block">
          <p className="pageInfo"> Profile</p>
          <h5 className="h5New">
            <Link style={{ color: "white" }} to="/dashboard">
              Dashboard
            </Link>{" "}
            / Profile
          </h5>
        </div>
      </div>
      <div>
        <div
          className="content"
          style={{ backgroundColor: "#f1f5f9", paddingTop: 15 }}
        >
          <div className={style.container}>
            <div className="row pb-5 ">
              <div
                className={`col-lg-4 col-xl-2 col-sm-12 d-none d-xl-block theiaStickySidebar pr-0`}
              >
                <DashboardSidebar />
              </div>
              <Drawer
                closable={false}
                width={300}
                placement="left"
                onClose={onClose}
                visible={visible}
                style={{ paddingTop: 0 }}
              >
                <div className="widget-profile pro-widget-content pt-0 pb-4">
                  <div className="profile-info-widget">
                    <img src="../Images/logo.png" height="50" alt="" />
                  </div>
                </div>
                <DashboardSidebar />
              </Drawer>

              {/* {sidebar ?
                                <div className={`d-block d-xl-none ${style.toggle}`}>
                                    <DashboardSidebar />
                                </div> : null
                            } */}
              <div className="col-lg-12 col-xl-9">
                <div className="card">
                  <div className="card-body">
                    <Form
                      onFinish={handleSubmit}
                      layout="vertical"
                      initialValues={{
                        FirstName: `${userData?.FirstName == null ? "" : userData?.FirstName
                          }`,
                        LastName: `${userData?.LastName == null ? "" : userData?.LastName
                          }`,
                        Email: `${userData?.Email == null ? "" : userData?.Email
                          }`,
                        phoneNumber: `${userData?.phoneNumber == null
                          ? ""
                          : userData?.phoneNumber
                          }`,
                        Address1: `${userData?.Address1 == null ? "" : userData?.Address1
                          }`,
                        City: `${userData?.City == null ? "" : userData?.City}`,
                        State: `${userData?.State == null ? "" : userData?.State}`,
                        PostalCode: `${userData?.PostalCode == null
                          ? ""
                          : userData?.PostalCode
                          }`,

                        // Country: `${userData?.Country == null ? config?.country : userData?.Country}`,
                        Country: `${config?.country}`,
                        Languages: `${userData?.Languages == null ? "" : userData?.Languages
                          }`,
                        gender: `${userData?.gender == null ? "" : userData?.gender
                          }`,
                        // DOB: `${userData?.DOB == moment() ? "" : moment(userData?.DOB)}`,
                        DOB: userData?.DOB ? moment(userData?.DOB) : null,
                        familyDoctor: `${familyD == null ? "" : familyD}`,
                      }}
                      validateMessages={validateMessages}
                    >
                      <div className="row">
                        <div className="col-sm-12 col-md-6 col-xl-6 ">
                          <div className="change-avatar pb-2">
                            <div className="profile-img">
                              {
                                imgLoader ?
                                  <div className={style.spinDiv}>
                                    <Spin />
                                  </div>
                                  :
                                  <>
                                    {ProfilePath ? (
                                      <img src={ProfilePath} alt="" />
                                    ) : (
                                      <img
                                        src={userData.profilepicture == null ? 'https://www.pngarts.com/files/6/User-Avatar-in-Suit-PNG.png' : getUserImage(userData.profilepicture)}
                                        alt="User"
                                      />
                                    )}
                                  </>
                              }
                            </div>
                            <div className="upload-img">
                              <div className="change-photo-btn">
                                <span>
                                  <FontAwesomeIcon
                                    className="mr-1"
                                    icon={faUpload}
                                  />{" "}
                                  Upload Photo
                                </span>
                                <input type="file" className="upload" onChange={userProffile} />
                              </div>
                              <small className="form-text text-muted">
                                Allowed JPG, GIF or PNG. Max size of 2MB
                              </small>
                            </div>
                          </div>
                          <div className={style.personHeading}>
                            <h4>Personal Info</h4>
                          </div>
                          <hr></hr>
                          <div className="row" style={{ marginTop: 15 }}>
                            <div className="col-6 ">
                              <div className={`form-group `}>
                                <Form.Item
                                  label="First Name"
                                  name="FirstName"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please Enter First Name!",
                                    },
                                  ]}
                                >
                                  <input
                                    type="text"
                                    className={`${style.inputbox}`}
                                    name="FirstName"
                                    onChange={handleChange}
                                  />
                                </Form.Item>
                              </div>
                            </div>
                            <div className="col-6">
                              <div className="form-group">
                                <Form.Item
                                  label="Last Name"
                                  name="LastName"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please Enter Last Name!",
                                    },
                                  ]}
                                >
                                  <input
                                    type="text"
                                    className={`${style.inputbox}`}
                                    name="LastName"
                                    onChange={handleChange}
                                  />
                                </Form.Item>
                              </div>
                            </div>
                          </div>
                          <div className={`form-group `}>
                            <Form.Item
                              name="Email"
                              label="Email"
                              rules={[{ required: true, type: "email" }]}
                            >
                              <input
                                type="text"
                                className={`${style.inputbox}`}
                                name="Email"
                                onChange={handleChange}
                              />
                            </Form.Item>
                          </div>
                          <div
                            className={`form-group `}
                            style={{ width: "100%" }}
                          >
                            <Form.Item
                              name="phoneNumber"
                              label="Phone Number"
                              rules={[
                                {
                                  required: true,
                                  message: "Please Enter phone number!",
                                },
                              ]}
                            >
                              <PhoneInput
                                enableSearch
                                country={"es"}
                                value={formValues.phoneNumber || ""}
                                onChange={(phone) => setPhoneNumber(phone)}
                              />
                            </Form.Item>
                            {/* <input  type="text" className={`${style.inputbox}`} name="phoneNumber" value={formValues.phoneNumber || ""} onChange={handleChange} /> */}
                          </div>

                          <div className="form-group">
                            <Form.Item
                              name="DOB"
                              label="Date of Birth"
                              rules={[
                                {
                                  required: true,
                                  message: "Please Enter Date Of Birth",
                                },
                              ]}
                            >
                              <DatePicker
                                size="large"
                                style={{ width: "100%" }}
                                format="YYYY/MM/DD"
                                onChange={onDateChange}
                              />
                              {/* <DatePicker autocomplete="off" dateFormat="yyyy-MM-dd" selected={startDate} onChange={(date) => setStartDate(date)} /> */}
                            </Form.Item>
                          </div>
                          <div className="form-group pb-0 mb-0">
                            <div className="row">
                              <div className="col-sm-6">
                                <Form.Item
                                  name="Languages"
                                  label="Language"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please Select your Language !",
                                    },
                                  ]}
                                >
                                  <select
                                    placeholder="Select Language"
                                    className={`select ${style.select}`}
                                    name="Languages"
                                    value={formValues.Languages || ""}
                                    onChange={handleChange}
                                  >
                                    <option>Select Languages</option>
                                    <option>English</option>
                                    <option>French</option>
                                    <option>Spanish</option>
                                    <option>Hindi</option>
                                  </select>
                                </Form.Item>
                              </div>
                              <div className="col-sm-6">
                                <Form.Item
                                  name="gender"
                                  label="Gender"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please Select your gender !",
                                    },
                                  ]}
                                >
                                  <select
                                    placeholder="Select gender"
                                    className={`select ${style.select}`}
                                    name="gender"
                                    value={formValues.gender || ""}
                                    onChange={handleChange}
                                  >
                                    <option>Select Gender</option>
                                    <option>male</option>
                                    <option>female</option>
                                    <option>intersex</option>
                                  </select>
                                </Form.Item>
                              </div>
                            </div>
                          </div>
                          <div
                            className="form-group"
                            style={{ height: "45px" }}
                          >
                            <Form.Item
                              name="familyDoctor"
                              label="Family Doctor"
                            >
                              {/* {familyDisplayId ? null : */}
                              <Select
                                showSearch
                                placeholder="Select Service"
                                className={style.selectMember}
                                optionFilterProp="children"
                                size="large"
                                style={{ width: "100%" }}
                                onChange={handleChangeMemeber}
                              >
                                {allDoctor?.map((itemm, index) => (
                                  <>
                                    {itemm.FirstName != null ? (
                                      <Option
                                        key={index}
                                        value={itemm?.companyemployeeid}
                                      >
                                        <img
                                          className={style.bgImg}
                                          src={itemm?.profilepicture}
                                          onError={(e: any) => {
                                            e.target.onerror = null;
                                            e.target.src =
                                              "https://doccure.dreamguystech.com/template/assets/img/doctors/doctor-thumb-02.jpg";
                                          }}
                                          alt=""
                                          height="20"
                                          width="20"
                                        />
                                        Dr. {itemm?.FirstName} {itemm?.LastName}
                                        <p className="p-0 m-0 pl-5">
                                          {itemm?.ClinicAddress}
                                        </p>
                                        {/* <div className={style.container_Dr}>
                                                                                    <div>
                                                                                        <img className="" src={itemm?.profilepicture}
                                                                                            onError={(e: any) => {
                                                                                                e.target.onerror = null;
                                                                                                e.target.src =
                                                                                                    "https://doccure.dreamguystech.com/template/assets/img/doctors/doctor-thumb-02.jpg";
                                                                                            }} alt="" />
                                                                                    </div>
                                                                                    <div className="pl-3">
                                                                                        <p className="p-0 m-0"> Dr. {itemm?.FirstName} {itemm?.LastName}</p>
                                                                                        <p className="p-0 m-0">{itemm?.ClinicAddress}</p>
                                                                                    </div>
                                                                                </div> */}
                                      </Option>
                                    ) : null}
                                  </>
                                ))}
                              </Select>

                              {/* {familyDisplayId ? */}
                              {/* <Select showSearch placeholder="Select Service" className={style.selectMember} optionFilterProp="children"
                                                                    defaultValue={familyDisplayId?.FirstName + " " + familyDisplayId?.MiddleName + " " + familyDisplayId?.LastName}
                                                                    size="large"
                                                                    style={{ width: "100%" }}
                                                                    onChange={handleChangeMemeber}
                                                                >
                                                                    {allDoctor?.map((itemm, index) => (
                                                                        <>
                                                                            {itemm.FirstName != null ?
                                                                                <Option key={index} value={itemm.companyemployeeid}>Dr. {itemm.FirstName} {itemm.LastName}</Option> :
                                                                                null}
                                                                        </>
                                                                    ))}
                                                                </Select>  */}
                            </Form.Item>
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-6 col-xl-6">
                          <div className={style.addressHeading}>
                            <h4>Address Info</h4>
                          </div>
                          <hr></hr>
                          <div className={`form-group `}>
                            <Form.Item
                              name="Address1"
                              label="Address"
                              rules={[
                                {
                                  required: true,
                                  message: "Please Enter your Address!",
                                },
                              ]}
                            >
                              <input
                                type="text"
                                className={`${style.inputbox}`}
                                name="Address1"
                                onChange={handleChange}
                              />
                            </Form.Item>
                          </div>
                          <div className="row">
                            <div className="col-6">
                              <div className={`form-group `}>
                                <Form.Item
                                  name="Country"
                                  label="Country"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please Select your Country!",
                                    },
                                  ]}
                                >
                                  {/* <input type="text" className={`${style.inputbox}`} name="Country" onChange={handleChange} /> */}
                                  <Select
                                    disabled
                                    size="large"
                                    showSearch
                                    placeholder="Select a person"
                                    optionFilterProp="children"
                                    onChange={onChangeCountry}
                                    onSearch={onSearchCountry}
                                    filterOption={(input, option) =>
                                      option.children
                                        .toLowerCase()
                                        .indexOf(input.toLowerCase()) >= 0
                                    }
                                  >
                                    {CountrysJson?.countries.map((element) => {
                                      return (
                                        <Option value={element?.country}>
                                          {element?.country}
                                        </Option>
                                      );
                                    })}
                                  </Select>
                                </Form.Item>
                              </div>
                            </div>
                            <div className="col-6">
                              <div className={`form-group `}>
                                <Form.Item
                                  name="State"
                                  label="State/Province"
                                  rules={[
                                    {
                                      required: true,
                                      message:
                                        "Please Select your State/Province!",
                                    },
                                  ]}
                                >
                                  <Select
                                    showSearch
                                    size="large"
                                    placeholder="Select a person"
                                    optionFilterProp="children"
                                    onChange={onChangeCity}
                                    onSearch={onSearchCity}
                                    filterOption={(input, option) =>
                                      option.children
                                        .toLowerCase()
                                        .indexOf(input.toLowerCase()) >= 0
                                    }
                                  >
                                    {selectCountry?.states?.map((element) => {
                                      return (
                                        <Option value={element}>
                                          {element}
                                        </Option>
                                      );
                                    })}
                                  </Select>
                                </Form.Item>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-6">
                              <div className="form-group">
                                <Form.Item
                                  name="PostalCode"
                                  label="Zip Code"
                                  rules={[
                                    {
                                      required: true,
                                      message: "PostalCode is not valid!",
                                      pattern: new RegExp(validPostCode),
                                    },
                                  ]}
                                >
                                  <input
                                    type="text"
                                    className={`${style.inputbox}`}
                                    name="PostalCode"
                                    onChange={handleChange}
                                  />
                                </Form.Item>
                              </div>
                            </div>
                            <div className="col-6">
                              <div className={`form-group `}>
                                <Form.Item
                                  name="City"
                                  label="City"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please Enter your City!",
                                    },
                                  ]}
                                >
                                  <input
                                    type="text"
                                    className={`${style.inputbox}`}
                                    onChange={handleChange}
                                  />
                                  {/* <input type="text" className={`${style.inputbox}`} name="City" onChange={handleChange} /> */}
                                </Form.Item>
                              </div>
                            </div>
                          </div>
                          <div className={style.insureHeading}>
                            <h4 className="pt-3">Insurance Info</h4>
                            <button
                              onClick={showModal}
                              type="button"
                              className="ant-btn ant-btn-dashed mx-4 "
                            >
                              <span>
                                {" "}
                                <FontAwesomeIcon
                                  className="mr-1 pr-1"
                                  icon={faPlus}
                                />
                                Add New
                              </span>
                            </button>
                          </div>
                          <hr
                            className="m-1 p-0"
                            style={{ marginBottom: 0 }}
                          ></hr>
                          <div className="form-group" style={{ height: 305 }}>
                            <Table
                              size="small"
                              columns={columns}
                              dataSource={insuranceData}
                              pagination={{ pageSize: 5 }}
                            />
                          </div>

                          {/* <div className="form-group">
                                                        <label className={style.inputLable}>Insurance provider</label>
                                                        <input  minLength={10} type="text" className={`${style.inputbox}`} name="insuranceProvider" defaultValue={formValues.insuranceProvider || ""} onChange={handleChange} />
                                                    </div> */}
                          <hr></hr>
                          <div className={style.div_btn}>
                            {loader ? (
                              <div className="spinner">
                                <Spin tip="Loading..."></Spin>
                              </div>
                            ) : (
                              <button type="submit" className="btn">
                                Update
                              </button>
                            )}
                          </div>
                        </div>
                      </div>
                    </Form>
                  </div>
                </div>
              </div>
            </div>

            {/* modal */}
            <Modal
              footer={null}
              title="Insurance Info"
              visible={isModalVisible}
              onCancel={handleCancel}
            >
              <Form form={form} onFinish={handleSubmitModal} layout="vertical">
                <div>
                  <label className={style.inputLable}>Insurance Type *</label>
                  <Select
                    defaultValue="PRIVATE_INSURANCE"
                    size="large"
                    style={{ width: "100%" }}
                    onChange={handleChangeSelect}
                  >
                    <Option value="PRIVATE_INSURANCE">Private insurance</Option>
                    <Option value="OHIP">OHIP</Option>
                  </Select>

                  <div className="row" style={{ marginTop: 20 }}>
                    <div className="col-6">
                      <div className="form-group">
                        <Form.Item
                          label="Insurance Number"
                          name="Insurancesvcnum"
                          rules={[
                            {
                              required: true,
                              message: "Please Enter Insurance Number!",
                            },
                            {
                              max: 10,
                              message: "must be maximum 10 characters.",
                            },
                          ]}
                        >
                          <Input
                            max={10}
                            size="large"
                            onChange={handleChange}
                          />
                        </Form.Item>

                        {/* <label className={style.inputLable}>Insurance Number *</label>
                                                <Input size="large" autoComplete="off" maxLength={10} type="text" className={`${style.inputbox}`} name="Insurancesvcnum" defaultValue={formValues.Insurancesvcnum || ""} onChange={handleChange} /> */}
                      </div>
                    </div>
                    {insuranceType == "OHIP" ? (
                      <>
                        <div className="col-6">
                          <div className="form-group">
                            <Form.Item
                              label="Name"
                              name="name"
                              rules={[
                                {
                                  required: true,
                                  message: "Please Enter Name!",
                                },
                              ]}
                            >
                              <Input size="large" onChange={handleChange} />
                            </Form.Item>

                            {/* <label className={style.inputLable}>Version code *</label>
                                                            <Input size="large" autoComplete="off" type="text" className={`${style.inputbox}`} name="insuranceVersionCode" defaultValue={formValues.insuranceVersionCode || ""} onChange={handleChange} /> */}
                          </div>
                        </div>
                        <div className="col-6">
                          <div className="form-group">
                            <Form.Item
                              label="Version code"
                              name="insuranceVersionCode"
                              rules={[
                                {
                                  required: true,
                                  message: "Please Enter Version code!",
                                },
                              ]}
                            >
                              <Input size="large" onChange={handleChange} />
                            </Form.Item>
                          </div>
                        </div>
                      </>
                    ) : (
                      <>
                        <div className="col-6">
                          <div className="form-group">
                            <Form.Item
                              label="Name"
                              name="name"
                              rules={[
                                {
                                  required: true,
                                  message: "Please Enter Name!",
                                },
                              ]}
                            >
                              <Input size="large" onChange={handleChange} />
                            </Form.Item>

                            {/* <label className={style.inputLable}>Name *</label>
                                                            <Input size="large" autoComplete="off" type="text" className={`${style.inputbox}`} name="name" onChange={handleChange} /> */}
                          </div>
                        </div>
                        <div className="col-6">
                          <div className="form-group">
                            <Form.Item
                              label="Provider"
                              name="provider"
                              rules={[
                                {
                                  required: true,
                                  message: "Please Enter Provider!",
                                },
                              ]}
                            >
                              <Input size="large" onChange={handleChange} />
                            </Form.Item>
                            {/* <label className={style.inputLable}>Provider *  </label>
                                                            <Input size="large" autoComplete="off" type="text" className={`${style.inputbox}`} name="provider" onChange={handleChange} /> */}
                          </div>
                        </div>
                      </>
                    )}
                  </div>
                </div>
                <div className="d-flex justify-content-end">
                  <Button
                    type="primary"
                    loading={btnLoader}
                    // onClick={() => handleOk()}
                    htmlType="submit"
                  >
                    Submit
                  </Button>
                </div>
              </Form>
            </Modal>
          </div>
        </div>
        <div className="footer">
          <Footer />
        </div>
      </div>
    </div>
  );
};

export default Profile;
