/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { useEffect, useState } from "react";
import { TopBar } from "components";
import { DashboardSidebar } from "./sidebar/sidebar.jsx";
import { Tab, Tabs, Button } from "react-bootstrap";
import moment from "moment";
import {
  Spin,
  Modal,
  notification,
  Table,
  Space,
  Drawer,
  Empty,
  Button as Buttons
} from "antd";
import { useHistory } from "react-router-dom";
import Footer from "components/footerFix";
import style from "./style.module.css";
import SSEHandler from "../../lib/SSEHandler";
import { GET, POST } from "../../services/common.api";
import { IMG01 } from "./img";
import IMG02 from "../../images/user.png";
import Man from "../../images/man.png";
import Woman from "../../images/woman.png";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendar,
  faEquals,
  faEye,
  faVideo,
} from "@fortawesome/free-solid-svg-icons";
import { PlusOutlined } from '@ant-design/icons';

import { Viewer, Worker } from '@react-pdf-viewer/core';
import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout';
import { base64toBlob } from "../../utils/common";
import PatientDetail from "../../components/patientDetail";
import { setAppConfig, setFamilyDoctor } from "../../redux/actions/userActions";


const Dashboard = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const defaultLayoutPluginInstance = defaultLayoutPlugin();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [loader, setLoader] = useState(false);
  const [loaderFamily, setLoaderFamily] = useState(false);
  const [priscriptionsDetails, setPriscriptionsDetails] = useState([]);
  const [callBackDetails, setCallBackDetails] = useState([]);


  const [selectAppointmentId, setSelectAppointmentId] = useState(0);
  const [doctorList, setdoctorList] = useState([]);
  const [PdfString, setPdfString] = useState("");
  // const [prescription, setPrescription] = useState([]);
  const [isDetailVisible, setIsDetailVisible] = useState(false);
  const [selectedCard, setSelectedCard] = useState("Upcoming");
  const [familyMembers, setFamilyMembers] = useState([]);
  const [showFamily, setShowFamily] = useState(false);
  const [selectedFamilyMembers, setSelectedFamilyMembers] = useState({} as any);
  let user = useSelector((state: any) => state.userReducer.user);
  let config = useSelector((state: any) => state.AppConfig.config);
  const isProfileComplate = useSelector((state: any) => state.userReducer.IsProfileComplate);


  const getFamilyMember = async () => {
    try {
      setLoaderFamily(true)
      const { data: { body: { FamilyMembers } } } = await GET(`userfamilymembers?id=${user.id}`);
      console.log(FamilyMembers, "FamilyMembers")
      if (FamilyMembers) {
        setLoaderFamily(true)
        setFamilyMembers(FamilyMembers);
        setLoaderFamily(false)
      } else {
        setLoaderFamily(true)
        setFamilyMembers([]);
        setLoaderFamily(false)
      }
    } catch (err) {
      setLoaderFamily(false)
      console.log("err: ", err);
    }
  };

  const patientDetailClose = () => {
    setIsDetailVisible(false);
  };

  const getFamilyDoctor = () => {
    try {
      GET(`patient/familydoc/${user.id}`).then((response) => {
        console.log("family member...: ", response?.data?.body?.CompanyEmployee?.CompanyEmployee);
        let N = response?.data?.body?.CompanyEmployee?.CompanyEmployee
        if (N !== null && N !== "") {
          dispatch(setFamilyDoctor(`Dr. ${N?.FirstName} ${N?.LastName}`))
        }
      });
    } catch (error) {
      notification.error({
        message: error.message,
      });
    }
  }

  useEffect(() => {
    if (user.id !== undefined) {
      getDoctortList({ showloader: false }, user?.id);
      getpriscriptionList(user.id);
      getcallBackList(user.id)
      getFamilyMember()
      getFamilyDoctor()
    }
  }, [user]);


  const getAppConfig = () => {
    try {

      GET("config").then((response) => {
        console.log("config...: ", response?.data[0]);
        dispatch(setAppConfig(response?.data[0]))
      });
    } catch (error) {
      notification.error({
        message: error.message,
      });
    }
  }

  useEffect(() => {
    window.scrollTo(0, 0);
    getAppConfig()

  }, []);

  const getpriscriptions = (item) => {
    console.log("btn click", item);
    setSelectAppointmentId(item);
    setIsDetailVisible(true);
  };

  const getpriscriptionList = (id) => {
    async function fetchMyAPI() {
      const prescription = await GET(
        `patientprescriptions?patientid=` + id
      );
      console.log(prescription.data, "prescription list data .........");
      setPriscriptionsDetails(prescription?.data);
    }
    fetchMyAPI();
  };

  const getcallBackList = (id) => {
    async function fetchMyAPI() {
      const prescription = await GET(
        `callbackrequests/patient/${id}`
      );
      setCallBackDetails(prescription?.data?.body?.data);
    }
    fetchMyAPI();
  };

  const getDoctortList = (options?: any, id?: any) => {
    console.log(id, "My id")
    async function fetchMyAPI() {
      if (!options || (options && options.showloader)) {
        // setLoader(true);
      }
      setLoader(true);
      const patientlist = await GET(`appointment?patientid=${id}`);
      // const patientlist = await GET(`appointment?patientid=148`);
      console.log(patientlist, "new api call dash");
      setLoader(false);
      setdoctorList(patientlist?.data);
    }
    fetchMyAPI();
  };

  const getDateFormate = (dateData) => {
    let stillUtc = moment.utc(dateData).toDate();
    return moment(stillUtc, ["YYYY-MM-DD h:mm:ss A"]).local().format("h:mm A");
  };

  const showPdf = async (preId) => {
    console.log("preId: ", preId);
    try {
      const paylod = {
        prescriptionId: preId,
      };
      POST("generatepdf", paylod).then((response) => {
        console.log("response: ", response);
        if (response?.data?.statusCode === 200) {
          const url = URL.createObjectURL(base64toBlob(response?.data?.body));
          console.log(url, "url pdf")
          setPdfString(url);
          setIsModalVisible(true);
        } else {
          notification.error({
            message: "Error while save prescription.",
          });
        }
      });
    } catch (error) {
      notification.error({
        message: error.message,
      });
    }
  };

  SSEHandler.getInstance().eventEmitter.on("updatedAppointmentList", () => {
    console.log("appointment updated successfully");
    getDoctortList({ showloader: false }, user?.id);
  });

  const appointmentListColumn = [
    {
      title: "Doctor",
      dataIndex: ["providername", "clinicname"],
      key: "name",
      render: (text, row) => (
        <a style={{ color: "#1A3045", fontSize: 15 }}>
          <div className="row">
            <div>
              <img
                style={{ width: 40, marginLeft: 8 }}
                className="avatar-img rounded-circle"
                // src={IMG01}
                src={row.providerprofilepicture == null ? IMG01 : getUserImage(row.providerprofilepicture)}

              />
            </div>
            <div style={{ paddingLeft: 13 }}>
              Dr. {row["providername"]}
              <p style={{ color: "#009efb", margin: 0 }}>{row["clinicname"]}</p>
            </div>
          </div>
        </a>
      ),
    },
    {
      title: "Appointment Date",
      dataIndex: "startdatetime",
      key: "name",
      render: (text, Speciality) => (
        <a style={{ fontSize: 15 }}>
          {moment(text).local().format("DD-MMM-YYYY")}{" "}
          <div>
            <p style={{ color: "#009efb", margin: 0 }}>
              {moment(text).utc().format('hh:mm A')}
            </p>
          </div>
        </a>
      ),
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      render: (tags) => (
        <>
          {tags == "DRAFT" ? (
            <span className={`badge badge-warning ${style.tag_dashboard2}`}>
              PENDING
            </span>
          ) : null}
          {tags == "PENDING" ? (
            <span className={`badge badge-warning ${style.tag_dashboard2}`}>
              CALLBACK
            </span>
          ) : null}
          {tags == "CONFIRMED" ? (
            <span className={`badge badge-success ${style.tag_dashboard}`}>
              CONFIRMED
            </span>
          ) : null}
          {tags == "COMPLETE" ? (
            <span className={`badge badge-secondary ${style.tag_dashboard}`}>
              CONFIRMED
            </span>
          ) : null}
          {tags == "REJECTED" ? (
            <span className={`badge badge-danger ${style.tag_dashboard}`}>
              REJECTED
            </span>
          ) : null}
        </>
      ),
    },
    {
      title: "Action",
      key: "action",
      dataIndex: "appoinment_id",
      render: (status, data) => (
        <Space size="middle">
          <button
            className={style.buttonView}
            role="button"
            onClick={() =>
              getpriscriptions(data)
            }
          >
            <FontAwesomeIcon icon={faEye} className="mr-2" />
            View
          </button>
          {
            data.status === "CONFIRMED" ? (
              <>
                {
                  moment(data.startdatetime).format("DD-MMM-YYYY") == moment().format("DD-MMM-YYYY") ?
                    <>
                      {
                        getDateFormate(data.startdatetime) == moment().format("h:mm A") ?
                          <button className={style.buttonJoinOriginal} role="button">
                            <FontAwesomeIcon icon={faVideo} className="mr-2" />
                            Join
                          </button> : null
                      }
                    </>
                    : null
                }
              </>
            ) : null
          }
        </Space >
      ),
    },
  ];

  const priscriptionsListColumn = [
    {
      title: "Appoinment Id",
      dataIndex: "appoinment_id",
      key: "appoinment_id",
    },
    {
      title: "Drug Name",
      dataIndex: "drug_name",
      key: "drug_name",
      width: 400,
    },
    {
      title: "Pharmacy Name",
      dataIndex: "pharmacy_name",
      key: "pharmacy_name",
    },
    {
      title: "Action",
      key: "action",
      dataIndex: "prescription_id",
      render: (prescription_id, data) => (
        <Space size="middle">
          <button
            style={{ paddingTop: 6, display: 'flex', justifyContent: 'center', alignItems: 'center' }}
            className={style.buttonJoin}
            role="button"
            onClick={() => getpriscriptions(data)}
          >
            <FontAwesomeIcon className="mr-2" icon={faCalendar} />
            Appointment
          </button>

          <button
            className={style.buttonView}
            role="button"
            onClick={() =>
              showPdf(data.prescription_id)
            }
          >
            <FontAwesomeIcon icon={faEye} className="mr-2" />
            View
          </button>
        </Space>
      ),
    },
  ];


  const callBackColumn = [
    {
      title: "PATIENT NAME",
      dataIndex: ["patientname", "patientprofilepicture"],
      key: "patientname",
      render: (text, row) => (
        <a style={{ color: "#1A3045", fontSize: 15 }}>
          <div className="row">
            <div>
              <img
                style={{ width: 40, marginLeft: 8 }}
                className="avatar-img rounded-circle"
                src={row.patientprofilepicture == null ? IMG02 : `${process.env.REACT_APP_ASSET_URL}/${user.profilepicture}`}

              />
            </div>
            <div style={{ paddingLeft: 13 }}>
              {row.patientname}
            </div>
          </div>
        </a>
      ),
    },
     {
      title: "DOCTOR NAME",
      dataIndex: ["providername", "clinicname"],
      key: "name",
      render: (text, row) => (
        <a style={{ color: "#1A3045", fontSize: 15 }}>
          <div className="row">
            <div>
              <img
                style={{ width: 40, marginLeft: 8 }}
                className="avatar-img rounded-circle"
                // src={IMG01}
                src={row.providerprofilepicture == null ? IMG01 : getUserImage(row.providerprofilepicture)}

              />
            </div>
            <div style={{ paddingLeft: 13 }}>
              Dr. {row["providername"]}
              <p style={{ color: "#009efb", margin: 0 }}>{row["clinicname"]}</p>
            </div>
          </div>
        </a>
      ),
    },
    {
      title: 'DATE',
      dataIndex: 'created_at',
      key: 'created_at',
      render: (text) => {
        return <div>{moment(text).format('ll')}</div>
      },
    },
    {
      title: 'TIME',
      dataIndex: 'callbacktime',
      key: 'callbacktime',
      render: (text) => {
        return <div>{text}</div>
      },
    },
    {
      title: 'STATUS',
      dataIndex: 'callbackstatus',
      key: 'callbackstatus',
      render: (tags) => (
        <>
          {tags == "ON CALL" ? (
            <span className={`badge badge-warning ${style.tag_dashboard2}`}>
              ON CALL
            </span>
          ) : null}
          {tags == "NEW" ? (
            <span className={`badge badge-secondary ${style.tag_dashboard2}`}>
              NEW
            </span>
          ) : null}
          {tags == "FULFILLED" ? (
            <span className={`badge badge-success ${style.tag_dashboard}`}>
              FULFILLED
            </span>
          ) : null}
        </>
      ),
    },
  ]

  const getUserImage = (imageUrl) => {
    if (imageUrl.match(/^http[^\?]*.(jpg|jpeg|gif|png|tiff|bmp)(\?(.*))?$/gim) != null) {
      return imageUrl
    }
    return `${process.env.REACT_APP_ASSET_URL}/${imageUrl}`
  }



  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  const ClickTab = (e) => {
    console.log(e);
    setSelectedCard(e)
  }

  const viewFamilyMembers = (event: any) => {
    console.log(event,'family member selected')
    var filtered = familyMembers.filter(function (value, index, arr) {
      return value.id == event.id;
    });
    const data = [{
      userName: `${user?.FirstName} ${user?.MiddleName} ${user?.LastName}`,
      id: user?.id,
      gender: user?.gender
    }]
    filtered = data
    console.log(filtered, "filtered");
    setFamilyMembers(filtered)
    setSelectedFamilyMembers(event)
    setShowFamily(true);
    getDoctortList({ showloader: false }, event?.familymemberid);
    getpriscriptionList(event.id)
    getcallBackList(event.id)
    if (event?.id == user?.id) {
      setShowFamily(false)
      getFamilyMember()
    }
  }

  return (
    <>
      <div>
        <TopBar />
        <div className="box">
          <div className={`${style.toggebtn} d-block d-xl-none`}>
            <i>
              <FontAwesomeIcon
                className="mr-1"
                icon={faEquals}
                onClick={showDrawer}
              />
            </i>
          </div>
          <div className="d-block d-xl-none">
            <p className="pageInfoM">Dashboard</p>
          </div>
          <div className="d-none d-xl-block">
            <p className="pageInfo">Dashboard</p>
          </div>
        </div>
        <div>
          <div className="content" style={{ backgroundColor: '#f1f5f9' }}>
            <div className="container-fluid">
              <div className="row pb-5 ">
                <div className={`col-lg-4 col-xl-2 col-sm-12 d-none d-xl-block theiaStickySidebar`}>
                  <DashboardSidebar />
                </div>
                <Drawer closable={false} width={300} placement="left" onClose={onClose} visible={visible} style={{ paddingTop: 0 }}>
                  <div className="widget-profile pro-widget-content pt-0 pb-4">
                    <div className="profile-info-widget">
                      <img src="../Images/logo.png" height="50" alt="" />
                    </div>
                  </div>
                  <DashboardSidebar />
                </Drawer>
                {/* {sidebar ? (
                  <div className={`d-block d-xl-none ${style.toggle}`}>
                    <DashboardSidebar />
                  </div>
                ) : null} */}
                <div className="col-lg-12 col-xl-10">
                  {isProfileComplate == "false" ? (
                    <div className="alert alert-warning mb-2" role="alert">
                      Please complete your profile!{" "}
                      <a
                        href="/profile"
                        className="pl-3"
                        rel="noopener noreferrer"
                      >
                        {" "}
                        Edit Profile
                      </a>
                    </div>
                  ) : null}


                  <div className={`row ${style.div_row_new}`} >
                    <div className="col-lg-8">
                      <div className="row">
                        <div className={`col-sm-12 col-md-5  col-lg-5 col-xl-5 ${style.profile_div}`}>
                          <div className={style.D_Profile_div}>
                            {user?.profilepicture != "" && user?.profilepicture != null ? (
                              <img className="profpic" src={`${process.env.REACT_APP_ASSET_URL}/${user.profilepicture}`} alt="User" />) : (
                              <img className="profpic" src={IMG02} alt="User" />)
                            }
                            {
                              showFamily == false ?
                                <>
                                  <h4 className="py-2 m-0">{user?.FirstName} {user?.MiddleName} {user?.LastName}</h4>
                                  <h5 className="text-secondary">{user?.Email}</h5>
                                </>
                                :
                                <>
                                  <h4 className="py-2 m-0">{selectedFamilyMembers?.userName}</h4>
                                  {/* <h5 className="text-secondary">{selectedFamilyMembers?.userName}</h5> */}
                                </>
                            }
                            <div className="text-white rounded pb-3 pt-3" style={{ backgroundColor: '#ffff', width: '80%' }}>
                              <div>
                                <div className="d-flex mb-1">
                                  <div className="text-uppercase mr-auto fontColor">Balance</div>
                                  <div className="fontColor">TOTAL</div>
                                </div>
                                <div className="d-flex mb-2">
                                  <div className="mr-auto fontColor">{config?.currency_symbol} 3,000</div>
                                  <div className="fontColor">{config?.currency_symbol} 5,000</div>
                                </div>
                                <div className="progress">
                                  <div
                                    className="progress-bar fontColor"
                                    style={{
                                      width: '60%',
                                      backgroundColor: '#194f85'
                                    }}
                                    role="progressbar"
                                    aria-valuenow={60}
                                    aria-valuemin={0}
                                    aria-valuemax={100}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                        <div className={`col-sm-12 col-md-7  col-lg-7 col-xl-7 ${style.Pro_info_div}`}>
                          {
                            showFamily == false ?
                              <>
                                <div className={`row pt-3 `}>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 ">
                                    <div className="d-flex flex-column justify-content-start align-items-start">
                                      <div>
                                        <p className="m-0 p-0 text-secondary">Gender</p>
                                        <p className="m-0 p-0">{user?.gender}</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4">
                                    <div>
                                      <p className="m-0 p-0 text-secondary">Birthday</p>
                                      <p className="m-0 p-0">{user?.DOB == null ? "" : moment(user?.DOB).format('YYYY-MM-DD')}</p>
                                    </div>
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4">
                                    <div>
                                      <p className="m-0 p-0 text-secondary">Phone</p>
                                      <p className="m-0 p-0 ">{user?.phoneNumber}</p>
                                    </div>
                                  </div>
                                </div>
                                <div className="row pt-3">
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column ">
                                    <div>
                                      <p className="m-0 p-0 text-secondary">Street Address</p>
                                      <p className="m-0 p-0">{user?.Address1} {user?.Address2} </p>
                                    </div>
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column  ">
                                    <div>
                                      <p className="m-0 p-0 text-secondary">City</p>
                                      <p className="m-0 p-0">{user?.City}</p>
                                    </div>
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column  ">
                                    <div>
                                      <p className="m-0 p-0 text-secondary"> State/Province</p>
                                      <p className="m-0 p-0">{user?.State}</p>
                                    </div>
                                  </div>

                                </div>
                                <div className="row pt-3">
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column">
                                    <div>
                                      <p className="m-0 p-0 text-secondary">Country</p>
                                      <p className="m-0 p-0 ">{user?.Country}</p>
                                    </div>
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column">
                                    <div>
                                      <p className="m-0 p-0 text-secondary">Zipcode</p>
                                      <p className="m-0 p-0 ">{user?.PostalCode}</p>
                                    </div>
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column">
                                    <div>
                                      <p className="m-0 p-0 text-secondary">Languages</p>
                                      <p className="m-0 p-0 ">{user?.Languages}</p>
                                    </div>
                                  </div>

                                </div>
                              </> :
                              <>
                                <div className={`row pt-3 `}>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 ">
                                    <div className="d-flex flex-column justify-content-start align-items-start">
                                      <div>
                                        <p className="m-0 p-0 text-secondary">Gender</p>
                                        <p className="m-0 p-0">{selectedFamilyMembers?.gender}</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4">
                                    <div>
                                      <p className="m-0 p-0 text-secondary">Birthday</p>
                                      <p className="m-0 p-0">{moment(selectedFamilyMembers?.dateOfBirth).format('YYYY-MM-DD')}</p>
                                    </div>
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4">
                                    {/* <div>
                                      <p className="m-0 p-0 text-secondary">Phone</p>
                                      <p className="m-0 p-0 ">{user?.phoneNumber}</p>
                                    </div> */}
                                  </div>
                                </div>
                                <div className="row pt-3">
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column ">
                                    <div>
                                      <p className="m-0 p-0 text-secondary">Street Address</p>
                                      <p className="m-0 p-0">{selectedFamilyMembers?.address} </p>
                                    </div>
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column  ">
                                    {/* <div>
                                      <p className="m-0 p-0 text-secondary">City</p>
                                      <p className="m-0 p-0">{user?.City}</p>
                                    </div> */}
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column  ">
                                    {/* <div>
                                      <p className="m-0 p-0 text-secondary">State</p>
                                      <p className="m-0 p-0">{user?.State}</p>
                                    </div> */}
                                  </div>

                                </div>
                                <div className="row pt-3">
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column">
                                    {/* <div>
                                      <p className="m-0 p-0 text-secondary">Country</p>
                                      <p className="m-0 p-0 ">{user?.Country}</p>
                                    </div> */}
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column">
                                    {/* <div>
                                      <p className="m-0 p-0 text-secondary">Zipcode</p>
                                      <p className="m-0 p-0 ">{user?.PostalCode}</p>
                                    </div> */}
                                  </div>
                                  <div className="col-sm-4 col-md-4 col-xl-4 col-lg-4 d-flex flex-column">
                                    {/* <div>
                                      <p className="m-0 p-0 text-secondary">Languages</p>
                                      <p className="m-0 p-0 ">{user?.Languages}</p>
                                    </div> */}
                                  </div>

                                </div>
                              </>
                          }

                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4">
                      <div className={style.file_container_div}>
                        {loaderFamily ? (
                          <div className="spinner">
                            <Spin tip="Loading..."></Spin>
                          </div>
                        ) : (
                          <>
                            <div className={style.file_container}>
                              <div>
                                {
                                  showFamily == false ?
                                    <h4 className="font-weight-bold">Family Members</h4> :
                                    <h4 className="font-weight-bold">Primary Member</h4>
                                }
                              </div>
                              <div>
                                {
                                  familyMembers.length > 0 ? "" :
                                    <Buttons onClick={() => history.push('/dependent')} className="p-0 m-0" type="primary" shape="circle">
                                      <div className="pb-1">
                                        <PlusOutlined />
                                      </div>
                                    </Buttons>
                                }
                              </div>
                            </div>
                            <div className={style.inner_div}>
                              {
                                familyMembers.length > 0 ?
                                  <>
                                    {
                                      familyMembers.map(el => {
                                        return (
                                          <div className={`${style.file_in}`}>
                                            <div className={style.details}>
                                              {
                                                el.gender == "male" ?
                                                  <img className={style.fileIcon} src={Man} alt="" /> :
                                                  <img className={style.fileIcon} src={Woman} alt="" />
                                              }
                                              <div>
                                                <p className="pl-3 m-0" style={{ width: 160 }}> {el?.userName}</p>
                                                <p className={`${style.lblName} pl-3 m-0`}> {el?.relationship}</p>
                                              </div>
                                            </div>
                                            <div className={style.relationship}>
                                              <p className="pb-1 mb-1 text-uppercase" style={{ width: 80 }}>{el?.relationship}</p>
                                            </div>
                                            <div className="">
                                              <button
                                                className={style.buttonView1}
                                                role="button"
                                                onClick={() => { viewFamilyMembers(el) }}
                                              >
                                                view
                                              </button>
                                            </div>
                                          </div>
                                        )
                                      })
                                    }
                                  </>
                                  :

                                  <>
                                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={
                                      <>
                                        <span>
                                          No Data
                                          {/* <a className="" href="dependent">Add</a> */}
                                        </span>
                                        {/* <Buttons type="primary" shape="circle">
                                          <PlusOutlined />
                                        </Buttons> */}
                                      </>
                                    } />

                                    {/* <div className="d-flex justify-content-center align-item-center flex-column h-100">
                                      <img src="Images/problems/cancel.png" alt="" className={style.NoDataImg} />
                                      <p className="pt-2">No Member Available</p>
                                    </div> */}
                                  </>
                              }
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className={style.card}>
                    <div className="card-body pt-0" style={{ marginBottom: 40, height: 520 }} >
                      <div className={style.Segment_Div}>
                        <div style={{ backgroundColor: '#eff1f7', display: 'flex', padding: 5, borderRadius: 10, cursor: 'pointer' }}>
                          <div className={`${selectedCard === "Upcoming" ? "tab_selected" : "tab_static"}`} onClick={() => ClickTab('Upcoming')}>
                            <p className="m-0 p-0" style={{ width: 200 }}>Upcoming Appointments</p>
                          </div>
                          <div className={`${selectedCard === "Prescriptions" ? "tab_selected" : "tab_static"}`} onClick={() => ClickTab('Prescriptions')}>
                            <p className="m-0 p-0" style={{ width: 200 }}>Prescriptions</p>
                          </div>
                          <div className={`${selectedCard === "CallBack" ? "tab_selected" : "tab_static"}`} onClick={() => ClickTab('CallBack')}>
                            <p className="m-0 p-0" style={{ width: 200 }}>CallBack Requests</p>
                          </div>
                        </div>
                      </div>
                      {loader ? (
                        <div className="spinner">
                          <Spin tip="Loading..."></Spin>
                        </div>
                      ) : (
                        <>
                          {
                            selectedCard == "Upcoming" ?
                              <div className="pl-3">
                                <Table
                                  size="small"
                                  pagination={{ defaultPageSize: 5, pageSizeOptions: ['30', '40'], showSizeChanger: false }}
                                  columns={appointmentListColumn}
                                  dataSource={doctorList}
                                  scroll={{ x: 1000 }}
                                />
                              </div>
                                :
                                selectedCard == "Prescriptions" ?
                                  <div className="pl-3">
                              <Table
                                dataSource={priscriptionsDetails}
                                size="small"
                                pagination={{ defaultPageSize: 5, pageSizeOptions: ['30', '40'], showSizeChanger: false }}
                                columns={priscriptionsListColumn}
                                scroll={{ x: 1000 }}
                                    />
                                  </div>
                                  :
                                  <Table
                                  size="small"
                                  pagination={{ defaultPageSize: 5, pageSizeOptions: ['30', '40'], showSizeChanger: false }}
                                  columns={callBackColumn}
                                  dataSource={callBackDetails}
                                  scroll={{ x: 1000 }}
                                />

                          }
                          <Modal
                            centered
                            visible={isModalVisible}
                            width={1000}
                            onOk={() => setIsModalVisible(false)}
                            onCancel={() => setIsModalVisible(false)}
                            footer={null}
                          >
                            {/* <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js"> */}
                            {/* <div style={{ height: "100%", width: '100%' }}> */}
                            {/* <Viewer fileUrl={PdfString} /> */}
                            {/* </div> */}
                            {/* </Worker> */}

                            <div className="pt-4">
                              <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js">
                                <Viewer
                                  fileUrl={PdfString}
                                  plugins={[
                                    // Register plugins
                                    defaultLayoutPluginInstance,
                                  ]}
                                />
                              </Worker>
                            </div>



                          </Modal>
                        </>
                      )}
                    </div>

                  </div>
                </div>
              </div>
              <PatientDetail
                // title="Appointment Detail"
                title=""
                open={isDetailVisible}
                close={patientDetailClose}
                appointmentId={selectAppointmentId}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="footer">
        <Footer />
      </div>
    </>
  );
};
export default Dashboard;
