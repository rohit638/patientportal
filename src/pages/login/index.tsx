import style from "./style.module.css";
import { Link, useHistory } from "react-router-dom";
import { Auth } from "aws-amplify";
import { useState } from "react";
import { useDispatch } from "react-redux";
import {
  loadCurrentAccount,
  authentication,
  setCognitoUserData,
} from "../../redux/actions/userActions";
import { Spinner } from "react-bootstrap";
import { Form, Input, Button, Checkbox, notification, Spin } from "antd";

// import { GET } from '../../services/common.api'
// import SSEHandler from "../../lib/SSEHandler";
// import getJwtToken from "../../lib/jwthelper";

// Amplify.configure({
//   Auth: {
//     // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
//     identityPoolId: "ca-central-1:1d52bcee-e2b7-492f-b03e-880099e5c8ab",

//     // REQUIRED - Amazon Cognito Region
//     region: "ca-central-1",

//     // OPTIONAL - Amazon Cognito Federated Identity Pool Region
//     // Required only if it's different from Amazon Cognito Region
//     identityPoolRegion: "ca-central-1",

//     // OPTIONAL - Amazon Cognito User Pool ID
//     userPoolId: "ca-central-1_wfazhhnBE",

//     // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
//     userPoolWebClientId: "58k6i4bk0m9j5of3vkt4a4c9ur",

//     // OPTIONAL - Enforce user authentication prior to accessing AWS resources or not
//     mandatorySignIn: false,
//   },
// });

// You can get the current config object
const currentConfig = Auth.configure();

const onFinishFailed = (errorInfo: any) => {
  console.log("Failed:", errorInfo);
};

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  // const [email, setEmail] = useState("");
  // const [password, setPassword] = useState("");
  const [loader, setLoader] = useState(false);

  const continuee = (event) => {
    // event.preventDefault();
    // let emailId = email.trim().split(/ +/).join(' ')
    // signIn(email, password);
    // signIn("wrx.patient@yopmail.com", "Welcome@123");
  };

  const signIn = async (values: any) => {
    setLoader(true);
    console.log("Success:", values);
    try {
      const user = await Auth.signIn(values.email, values.password);
      console.log(user, "ppppp");
      if (user.challengeName === "NEW_PASSWORD_REQUIRED") {
        dispatch(setCognitoUserData(user));
        history.push("/complete-new-password");
      } else {
        if (user.username) {
          dispatch(loadCurrentAccount());
          dispatch(authentication(true));
          setLoader(false);
          history.push("/dashboard");
        }
      }
    } catch (error) {
      console.log("error: ", error);
      setLoader(false);
      notification.error({
        message: "Error",
        duration: 2,
        description: "Invalid Username or Password",
        onClick: () => {
          console.log("Notification Clicked!");
        },
      });
    }
  };

  /* eslint-disable no-template-curly-in-string */
  const validateMessages = {
    required: "${label} is required!",
    types: {
      email: "${label} is not a valid email!",
      number: "${label} is not a valid number!",
    },
  };
  /* eslint-enable no-template-curly-in-string */

  return (
    <Form
      name="basic"
      onFinish={signIn}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      layout="vertical"
      validateMessages={validateMessages}
    >
      <div className={style.div_container}>
        <div className={style.div_content}>
          <div className={style.div_image}>
            <div>
              {/* <p className="p_name">Welcome</p> */}
              <p className={style.p_name}>Sign in to Continue in ONRx</p>
            </div>
            <div className={style.div_round}>
              <img
                src="Images/logo.png"
                className={style.imgFluid}
                alt="Doccure Login"
              />
            </div>
          </div>

          <div className={style.div_form}>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  type: "email",
                  message: "Please enter your email!",
                },
              ]}
            >
              <Input size="large" placeholder="Email" />
            </Form.Item>

            {/* <Form.Item
              name="Email"
              rules={[{ required: true, message: 'Please input your Email!' }]}
            >
              <div className="form-group form-focus">
                <input className="form-control floating" onChange={(text: any) => setEmail(text.target.value)} />
                <label className="focus-label">Email</label>
              </div>
            </Form.Item> */}

            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password size="large" placeholder="Password" />
              {/* <div className="form-group form-focus">
                <input type="password" className="form-control floating" onChange={(text: any) => setPassword(text.target.value)} />
                <label className="focus-label">Password</label>
              </div> */}
            </Form.Item>

            <div className="text-right">
              <Link to="/forgot-password" className="forgot-link">
                Forgot Password ?
              </Link>
            </div>

            {!loader ? (
              <button
                className={`${style.btnDashboard} btn btn-primary btn-block`}
                type="submit"
              >
                Login
              </button>
            ) : (
              <button className="btn btn-primary btn-block" type="submit">
                <Spinner
                  animation="border"
                  role="status"
                  style={{ color: "white", width: 25, height: 25 }}
                />
              </button>
            )}
          </div>
        </div>
        <div className={`text-center ${style.dontHave}`}>
          Don’t Have an Account ?
          <Link to="/register" style={{ color: "#1a6ab1" }}>
            {" "}
            Signup Now
          </Link>
        </div>
        <div className={`text-center  ${style.copy}`}>© 2021 ONRx</div>
      </div>
    </Form>
  );
};

export default Login;
