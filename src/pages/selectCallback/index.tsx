import { useEffect, useState } from "react";
import { TopBar } from "components";
import Footer from "components/footer";
import { Steps, Form, Button } from 'antd';
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { callbackAppointment, selectAppointmentType } from "../../redux/actions/userActions";
import style from "./style.module.css";
import moment from "moment";
import TextArea from "antd/lib/input/TextArea";

const SelectCallback = () => {
    const dispatch = useDispatch();
    const { Step } = Steps;
    const [selectedCard, setSelectedCard] = useState("");
    const [todayDate, setTodayDate] = useState("");
    const [certainHours, setCertainHours] = useState("");
    const [reason, setReason] = useState("");
    const [Comments, setComments] = useState("");
    const [slots, setSlots] = useState(false);
    const [slotsTime, setSlotsTime] = useState("")
    const history = useHistory();

    const cardClick = (selectedCard) => {
        console.log(selectedCard)
        setSelectedCard(selectedCard)
        dispatch(callbackAppointment(selectedCard));
        if (selectedCard == "Morning") {
            setSlotsTime("08:00AM-12:00PM")
        } else if (selectedCard == "Afternoon") {
            setSlotsTime("12:00PM-05:00PM")
        } else if (selectedCard == "Evening") {
            setSlotsTime("05:00PM-09:00AM")
        }
    };

    useEffect(() => {
        setTodayDate(moment().format("dddd, MMM DD, YYYY"))
        dispatch(callbackAppointment(selectedCard));
        generateGreetings()
    }, [])





    const generateGreetings = () => {
        let currentHour: any = moment().format("HH");
        if (currentHour >= 3 && currentHour < 12) {
            setCertainHours("Morning")
            console.log(currentHour, "event****")
            setSlots(false)
            setSlotsTime("08:00AM-12:00PM")

        } else if (currentHour >= 12 && currentHour < 15) {
            setCertainHours("Afternoon")
            setSelectedCard('Afternoon')
            setSlotsTime("12:00PM-05:00PM")
            console.log(currentHour, "event****")
            setSlots(false)

        } else if (currentHour >= 15 && currentHour < 20) {
            setCertainHours("Evening")
            console.log(currentHour, "event****")
            setSelectedCard('Evening')
            setSlotsTime("05:00PM-09:00AM")
            setSlots(false)
        } else {
            setSlots(true)
            console.log("NO slots available")
        }
    }

    const onFinish = (values: any) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    const leftArraow = () => {
        let C = moment().format("dddd, MMM DD, YYYY")
        setTodayDate(moment(C).format("dddd, MMM DD, YYYY"))
        generateGreetings()
    }
    const rightArraow = () => {
        let C = moment().format("DD-MM-YYYY")
        let A = moment(C, "DD-MM-YYYY").add(1, 'days');
        console.log(moment(A).format("dddd, MMM DD, YYYY"));
        setTodayDate(moment(A).format("dddd, MMM DD, YYYY"))
        setSlots(false)
    }



    return (
        <div style={{ height: '100%' }}>
            <TopBar />
            <div className={style.box}>
                <p className={style.pageInfo}>Providers</p>
                <h5 className={style.h5New}>Dashboard / Select Provider</h5>
            </div>

            <div className="content contentSelect" style={{ height: 600 }}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-0 col-md-1 col-xl-2"></div>
                        <div className="col-sm-12 col-md-10 col-xl-8">
                            <div className="stepDiv d-none d-sm-block">
                                <Steps>
                                    <Step status="finish" title="Payment" />
                                    {/* <Step status="wait" title="Problems" /> */}
                                    <Step status="finish" title="Select Type" />
                                    <Step status="finish" title="Select Slot" />
                                    {/* <Step status="wait" title="Select Doctor" /> */}
                                    {/* <Step status="wait" title="Book Appointment" /> */}
                                </Steps>
                                <div className={style.divPayment}>
                                    {
                                        todayDate !== moment().format('dddd, MMM DD, YYYY') ?
                                            <img onClick={leftArraow} src="Images/problems/arrow-left.png" alt="" className={style.arrowIcon} /> :
                                            null
                                    }
                                    <h2 className="lblPayment px-3">{todayDate}</h2>
                                    {
                                        todayDate == moment().format('dddd, MMM DD, YYYY') ?
                                            <img onClick={rightArraow} src="Images/problems/arrow-right.png" alt="" className={style.arrowIcon} /> :
                                            null
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-0 col-md-1 col-xl-2"></div>
                    </div>

                    <div className="row">
                        <div className="col-12">
                            <div className="row text-left">
                                <div className="col-sm-1 col-md-1 col-xl-2"></div>
                                <div className="col-sm-10 col-md-10 col-xl-8">
                                    <div className={style.div_flex}>
                                        {
                                            certainHours == 'Morning' && todayDate == moment().format('dddd, MMM DD, YYYY') ?
                                                <>
                                                    <div className={style.div_inner_flex}>
                                                        <div onClick={() => cardClick('Morning')} className={`${selectedCard === 'Morning' ? `${style.card_selected}` : `${style.card_static}`}`}>
                                                            <img src="Images/morning.png" alt="" className={style.productImg} />
                                                            <div className="doc-info-center text-center">
                                                                <h4 className="p-0 m-0">Morning</h4>
                                                                <h6 className="p-0 m-0">8am - 12pm</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className={style.div_inner_flex}>
                                                        <div className={`${selectedCard === 'Afternoon' ? `${style.card_selected}` : `${style.card_static}`}`} onClick={() => cardClick('Afternoon')}>
                                                            <img src="Images/afternoon.png" alt="" className={style.productImg} />
                                                            <div className="doc-info-center text-center">
                                                                <h4 className="p-0 m-0">Afternoon</h4>
                                                                <h6 className="p-0 m-0">12pm - 5pm</h6>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className={style.div_inner_flex}>
                                                        <div className={`${selectedCard === 'Evening' ? `${style.card_selected}` : `${style.card_static}`}`} onClick={() => cardClick('Evening')}>
                                                            <img src="Images/evening.png" alt="" className={style.productImg} />
                                                            <div className="doc-info-center text-center">
                                                                <h4 className="p-0 m-0">Evening</h4>
                                                                <h6 className="p-0 m-0">5pm - 9am</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                                : null
                                        }
                                        {/* next day */}
                                        {
                                            todayDate !== moment().format('dddd, MMM DD, YYYY') ?
                                                <>
                                                    <div className={style.div_inner_flex}>
                                                        <div onClick={() => cardClick('Morning')} className={`${selectedCard === 'Morning' ? `${style.card_selected}` : `${style.card_static}`}`}>
                                                            <img src="Images/morning.png" alt="" className={style.productImg} />
                                                            <div className="doc-info-center text-center">
                                                                <h4 className="p-0 m-0">Morning</h4>
                                                                <h6 className="p-0 m-0">8am - 12pm</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className={style.div_inner_flex}>
                                                        <div className={`${selectedCard === 'Afternoon' ? `${style.card_selected}` : `${style.card_static}`}`} onClick={() => cardClick('Afternoon')}>
                                                            <img src="Images/afternoon.png" alt="" className={style.productImg} />
                                                            <div className="doc-info-center text-center">
                                                                <h4 className="p-0 m-0">Afternoon</h4>
                                                                <h6 className="p-0 m-0">12pm - 5pm</h6>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className={style.div_inner_flex}>
                                                        <div className={`${selectedCard === 'Evening' ? `${style.card_selected}` : `${style.card_static}`}`} onClick={() => cardClick('Evening')}>
                                                            <img src="Images/evening.png" alt="" className={style.productImg} />
                                                            <div className="doc-info-center text-center">
                                                                <h4 className="p-0 m-0">Evening</h4>
                                                                <h6 className="p-0 m-0">5pm - 9am</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                                : null
                                        }

                                        {
                                            certainHours == 'Afternoon' && todayDate == moment().format('dddd, MMM DD, YYYY') ?
                                                <>
                                                    <div className={style.div_inner_flex}>
                                                        <div className={`${selectedCard === 'Afternoon' ? `${style.card_selected}` : `${style.card_static}`}`} onClick={() => cardClick('Afternoon')}>
                                                            <img src="Images/afternoon.png" alt="" className={style.productImg} />
                                                            <div className="doc-info-center text-center">
                                                                <h4 className="p-0 m-0">Afternoon</h4>
                                                                <h6 className="p-0 m-0">12pm - 5pm</h6>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className={style.div_inner_flex}>
                                                        <div className={`${selectedCard === 'Evening' ? `${style.card_selected}` : `${style.card_static}`}`} onClick={() => cardClick('Evening')}>
                                                            <img src="Images/evening.png" alt="" className={style.productImg} />
                                                            <div className="doc-info-center text-center">
                                                                <h4 className="p-0 m-0">Evening</h4>
                                                                <h6 className="p-0 m-0">5pm - 9am</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                                : null
                                        }
                                        {
                                            certainHours == 'Evening' && todayDate == moment().format('dddd, MMM DD, YYYY') ?
                                                <>
                                                    <div className={style.div_inner_flex}>
                                                        <div className={`${selectedCard === 'Evening' ? `${style.card_selected}` : `${style.card_static}`}`} onClick={() => cardClick('Evening')}>
                                                            <img src="Images/evening.png" alt="" className={style.productImg} />
                                                            <div className="doc-info-center text-center">
                                                                <h4 className="p-0 m-0">Evening</h4>
                                                                <h6 className="p-0 m-0">5pm - 9am</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                                : null
                                        }

                                        {
                                            slots == true ?
                                                <div className={style.div_inner_flex}>
                                                    <div className={style.card_static}>
                                                        <img src="Images/problems/cancel.png" alt="" className={style.productImg} />
                                                        <div className="doc-info-center text-center">
                                                            <h4 className="p-0 m-0 pt-2">No slots available</h4>
                                                            {/* <h6 className="p-0 m-0">5pm - 12am</h6> */}
                                                        </div>
                                                    </div>
                                                </div>
                                                : null
                                        }
                                    </div>
                                </div>
                                <div className="col-sm-1 col-md-1 col-xl-2"></div>
                            </div>
                        </div>
                    </div>
                    <div className={style.reasonDiv}>
                        {
                            slots !== true ?
                                <div className={` mt-5`} style={{ width: 580, marginRight: 26 }}>
                                    <div className="pb-0 mb-0" style={{ textAlign: 'end' }}>
                                        <Form
                                            layout="vertical"
                                            name="basic"
                                            // labelCol={{ span: 8 }}
                                            // wrapperCol={{ span: 16 }}
                                            // initialValues={{ remember: true }}
                                            onFinish={onFinish}
                                            onFinishFailed={onFinishFailed}
                                            autoComplete="off"
                                        >

                                            <Form.Item
                                                label="Reason"
                                                name="Reason"
                                                rules={[{ required: true, message: 'Please Enter your Reason!' }]}
                                            >
                                                <div>
                                                    <TextArea placeholder="Enter Reason" onChange={(e) => setReason(e.target.value)} />
                                                </div>
                                            </Form.Item>
                                            {/* <Form.Item
                                        label="comments"
                                        name="comments"
                                        rules={[{ required: true, message: 'Please Enter your comments!' }]}
                                    >
                                        <div>
                                            <TextArea placeholder="Enter comments" onChange={(e) => setComments(e.target.value)} />
                                        </div>
                                    </Form.Item> */}
                                            {/* <Form.Item>
                                        <Button htmlType="submit" type="primary">Submit</Button>
                                    </Form.Item> */}
                                        </Form>
                                    </div>
                                </div>
                                : null
                        }
                    </div>

                </div>
            </div>

            <div className="footer">
                <Footer
                    // location="searchDoctor"
                    pageName="Submit"
                    hide="true"
                    valueReason={reason}
                    appoimentDate={todayDate}
                    disbleFooter={!slots && selectedCard !== "" ? true : false}
                    slotsTime={slotsTime}
                    selectslot={selectedCard}
                // Comments={Comments}
                />

                {/* <Footer
                    // location="problems"
                    location="appoiment-type"
                    pageName="serch"
                /> */}
            </div>
        </div>
    );
};

export default SelectCallback;