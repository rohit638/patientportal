
export { default as IMG01 } from '../../images/doc_not_found.png';
export { default as IMG02 } from '../../images/doctor-thumb-02.jpg';
export { default as IMG03 } from '../../images/doctor-thumb-03.jpg';
export { default as IMG04 } from '../../images/doctor-thumb-04.jpg';
export { default as IMG05 } from '../../images/doctor-thumb-05.jpg';

export { default as IMG06 } from '../../images/doctor-thumb-06.jpg';
export { default as IMG07 } from '../../images/doctor-thumb-07.jpg';
export { default as IMG08 } from '../../images/doctor-thumb-08.jpg';
export { default as IMG09 } from '../../images/doctor-thumb-09.jpg';
export { default as IMG10 } from '../../images/doctor-thumb-10.jpg';
