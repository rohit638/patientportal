import { useEffect, useState } from "react";
import { Steps, Button, Modal, Form, Input, Cascader, Typography, Radio, Table, Tag, Space, Tooltip, Popconfirm, Spin } from "antd";
import { useHistory, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import uuid from "react-uuid";
import SSEHandler from "../../lib/SSEHandler";
import WebrtcCallHandler from "../../lib/WebrtcCallHandler";
import style from "./style.module.css";
import Footer from "components/footer";
import { AppointmentRequestType, selectAppointmentType, setSelectedFamilyMember } from "../../redux/actions/userActions";
import * as VirtualWaitingRoomService from '../../services/virtualWaitingRoom'
import { GET } from "services/common.api";
import AddFamilyMember from "components/addFamilyMember";
import { EditOutlined } from "@material-ui/icons";

let callPayload: any;

const AppoimentType = () => {
  const { Step } = Steps;
  const { Text } = Typography;
  const dispatch = useDispatch();
  const history = useHistory();
  const whichVersion = useSelector(
    (state: any) => state.curentAppointment.whichVersion
  );
  const currentUserDetails: any = useSelector(
    (state: any) => state.userReducer.user
  );
  const familyMemberState: any = useSelector((state: any) => state?.curentAppointment?.familyMember);
  const currentAppointmentState: any = useSelector((state: any) => state?.curentAppointment?.appointmentRequestType);
  // const { showVirtualWaitingRoom } = useSelector(
  //   (state: any) => state.meeting
  //   );
  const appointmentTypeNew = useSelector((state: any) => state);
  const [showVirtualWaitingRoom, setShowVirtualWaitingRoom] = useState(false)
  const [selectedCard, setSelectedCard] = useState("fastService");
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [invocation, setInvocation] = useState<any>({});
  const [invocationLoading, setInvocationLoading] = useState(false);
  const [isCallBack, setCallBack] = useState(Boolean)
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedWalkInVisit, setSelectedWalkInVisit] = useState(null);
  const [loader, setLoader] = useState(false);
  const [familyMember, setFamilyMember] = useState([]);
  const [updateFamilyMember, setUpdateFamilyMember] = useState([]);
  const [isOwn, setIsOwn] = useState(true);
  const [familyModel, setFamilyModel] = useState(false);
  const [selectAppointment, setSelectAppointment] = useState("Own");


  const familyMembers = [
    { label: "self", value: "self" },
    { label: "person1", value: "person1" },
  ];

  const cardClick = (selectedCard) => {
    console.log(selectedCard)
    if (selectedCard === "Today") {
      setSelectedCard(selectedCard);
      dispatch(selectAppointmentType("other"))
      // history.push("/searchDoctor");
    } else if (selectedCard === "callback") {
      setCallBack(true)
      setSelectedCard(selectedCard);
      dispatch(selectAppointmentType("CallBackRequest"))
    }
    else {
      setSelectedCard("fastService")
      dispatch(selectAppointmentType("fastService"))
      startVirtualWaitingRoom()
      // setIsOpenModal(true);
    }
  };
  // const goToBack = () => {
  //   history.goBack();
  // };
  // const handleOk = () => {
  //   setIsOpenModal(false);
  // };
  const handleCancel = () => {
    WebrtcCallHandler.getInstance().cleanUp();
    //setIsOpenModal(false);
    if (invocation) {
      let requestObject = {
        invocationId: invocation.appointmentId,
        eventId: invocation.appointmentId,
        callId: invocation.callId,
      };
      setInvocationLoading(true);
      VirtualWaitingRoomService.cancelVirtualWaitingRoom(requestObject).then(result => {
        setInvocationLoading(false);
        // dispatch({
        //   type: "SHOW_VIRTUAL_WAITING_ROOM",
        //   payload: false,
        // });
        setShowVirtualWaitingRoom(false)
      }).catch(error => {
        console.log('error: ', error);
        setInvocationLoading(false);
      })
    }
  };
  const selectWalkInVisit = (walkInVisit) => {
    setSelectedWalkInVisit(walkInVisit);
  };


  const startVirtualWaitingRoom = () => {
    console.log("Loggeded !!!!!!")
    const roomId = uuid();
    console.log('roomId: ', roomId);

    const walkinInRequest = {
      "patientId": currentUserDetails?.sub,
      "bookedBy": currentUserDetails.id,
      "detail": "Virtual waiting room",
      "patientDetails": {
        ...currentUserDetails
      },
      "appointmentId": roomId,
      "callType": "video",
      "roomId": roomId,
      "callId": uuid(),
      "createdAt": new Date().getTime()
    }

    WebrtcCallHandler.getInstance().addExtraListenerWithForcefullyAdded(
      "onUserJoined",
      function (participant) {
        callPayload = {
          type: "video",
          isSFURequired: false,
          callInfo: {
            isSFURequired: false,
            toUsers: [
              {
                userId: participant.userData?.sub,
                name: participant.userData.name,
                avatar: "",
                userType: "doctor",
              },
            ],
          },
          fromUser: {
            userId: currentUserDetails?.sub,
            name: currentUserDetails.name,
            avatar: "",
            userType: "paitent",
          },
        };
        SSEHandler.getInstance().eventEmitter.emit(
          "ShowCallUI",
          callPayload
        );
        // dispatch({
        //   type: "SHOW_VIRTUAL_WAITING_ROOM",
        //   payload: false,
        // });
        setShowVirtualWaitingRoom(false)
      },
      false
    );

    WebrtcCallHandler.getInstance().addExtraListenerWithForcefullyAdded(
      "onConnected",
      function () {
        WebrtcCallHandler.getInstance()
          .getMeetingHandler()
          .startMeeting();

        VirtualWaitingRoomService.startVirtualWaitingRoom(walkinInRequest).then(result => {
          console.log('result: ', result);
          if (result) {
            let appointment = result;
            setInvocationLoading(false);
            setInvocation(appointment);
          }
        }).catch(error => {
          setInvocationLoading(false);
          // dispatch({
          //   type: "SHOW_VIRTUAL_WAITING_ROOM",
          //   payload: false,
          // });
          setShowVirtualWaitingRoom(false)
        })
      },
      false
    );

    SSEHandler.getInstance().onNewCallRequest({
      roomId: roomId,
      isSFURequired: false,
      type: "video",
    });

    // dispatch({
    //   type: "SHOW_VIRTUAL_WAITING_ROOM",
    //   payload: true,
    // });
    setShowVirtualWaitingRoom(true);

    setInvocationLoading(true)
  }

  useEffect(() => {
    console.log(currentAppointmentState)
    // dispatch(setSelectedFamilyMember([]))
    window.scrollTo(0, 0);
    dispatch(AppointmentRequestType("Own"))
    if (appointmentTypeNew?.curentAppointment?.appointmentType == 'other') {
      setSelectedCard("Today");
    } else if (appointmentTypeNew?.curentAppointment?.appointmentType == 'CallBackRequest') {
      setSelectedCard("callback");
    } else {
      setSelectedCard("fastService");
    }
  }, []);
  const handleCancelVirtualRoom = () => {
    setShowVirtualWaitingRoom(false)
  };

  const checkFamilyMembar = (event) => {
    setSelectAppointment(event.target.value);
    console.log(event.target.value)
    if (event.target.value == "familyMember") {
      setIsOwn(false);
      dispatch(AppointmentRequestType("familyMember"))
      setIsModalVisible(true);
      getFamilyMember()
    } else {
      setIsOwn(true);
      dispatch(AppointmentRequestType("Own"))
    }
  }



  const handleOkFamily = () => {
    setIsModalVisible(false);
  };

  const handleCancelFamily = () => {
    setIsModalVisible(false);
    setIsOwn(true)
    setSelectAppointment("Own")
  };

  const getFamilyMember = async () => {
    try {
      setLoader(true)
      const { data: { body: { FamilyMembers } } } = await GET(`userfamilymembers?id=${currentUserDetails.id}`);
      if (FamilyMembers) {
        setLoader(true)
        setFamilyMember(FamilyMembers);
        setLoader(false)
      } else {
        setLoader(true)
        setFamilyMember([]);
        setLoader(false)
      }
    } catch (err) {
      setLoader(false)
      console.log("err: ", err);
    }
  };

  const data = [
    {
      title: 'Family Member',
      dataIndex: 'userName',
      key: 'userName',
      render: userName =>
        <>
          {userName}
        </>,
    },
    {
      title: 'Relationship',
      dataIndex: 'relationship',
      key: 'relationship',
      render: relationship =>
        <Tag color="blue" style={{ borderRadius: 5 }}>
          <p style={{ margin: 0 }}>{relationship}</p>
        </Tag >
    },
    {
      title: 'Date Of Birth',
      dataIndex: 'dateOfBirth',
      key: 'dateOfBirth',
    },
    {
      title: 'Gender',
      dataIndex: 'gender',
      key: 'gender',
    },
    // {
    //   title: 'Address',
    //   dataIndex: 'address',
    //   key: 'address',
    //   render: (text) => <>
    //     <p style={{ width: 140, whiteSpace: "pre-wrap" }}>{text}</p>
    //   </>
    // },
    {
      title: 'Action',
      key: 'action',
      dataIndex: '',
      render: (data) => (
        <Space size="middle">
          <Tooltip placement="bottom" title="Select Member" key={'#1a6ab1'}>
            <Button onClick={() => {
              dispatch(setSelectedFamilyMember(data))
              setIsModalVisible(false)
            }} shape="round" type="primary" size="small">
              Select
            </Button>
            {/* <button className="button-Edit-Icon" role="button"
              onClick={() => {
                dispatch(setSelectedFamilyMember(data))
                setIsModalVisible(false)
              }}
            >
              select
              <FontAwesomeIcon icon={faPlus} />
            </button> */}
          </Tooltip>
        </Space >
      ),
    },
  ];

  const editFamilyMember = () => {
    setIsModalVisible(true)
    getFamilyMember()
  }

  const familyModelClose = async () => {
    setFamilyModel(false);
    try {
      setLoader(true)
      const { data: { body: { FamilyMembers } } } = await GET(`userfamilymembers?id=${currentUserDetails.id}`);
      if (FamilyMembers) {
        setLoader(true)
        console.log(FamilyMembers, "familyMembers");
        console.log(familyMemberState, "familyMemberState.familymemberid");
        var result = FamilyMembers.filter(obj => {
          return obj.id === familyMemberState.id
        })
        console.log(result[0], "result----");
        dispatch(setSelectedFamilyMember(result[0]))
        setLoader(false)
      } else {
        setLoader(true)
        setFamilyMember([]);
        setLoader(false)
      }
    } catch (err) {
      setLoader(false)
      console.log("err: ", err);
    }
  };

  const familyModelOpen = () => {
    setFamilyModel(true);
    setSelectedFamilyMember({});
    // setClick(false);
  };



  return (
    <div>
      {/* <TopBar /> */}
      <div className="box" style={{ marginTop: 0 }}>
        <p className="pageInfo">Appointment Type</p>
        <h5 className="h5New"><Link style={{ color: "white" }} to="/dashboard" >Dashboard</Link> / Appointment Type</h5>
      </div>
      <Modal
        visible={showVirtualWaitingRoom}
        title="Virtual Waiting Room"
        closable={true}
        width={1000}
        footer={null}
        onCancel={handleCancelVirtualRoom}
      >

        <div className="row">
          <div className="col-md-12">
            <div className="text-center">
              <img alt="Virtual Waiting Room" src="../Images/virtualWaitingRoom.png" />
            </div>
            <div className="text-center mt-2">
              <Button type="primary" size="large" danger={true} onClick={handleCancel} loading={invocationLoading} disabled={invocationLoading}>Cancel</Button>
            </div>
          </div>
        </div>

      </Modal>
      <div className="content contentSelect" style={{ height: 600 }}>
        <div className="container-fluid pb-4">
          <div className="row">
            <div className="col-sm-1 col-md-1 col-xl-1"></div>
            <div className="col-sm-10 col-md-10 col-xl-10">
              <div className="stepDiv d-none d-sm-block">
                <Steps size="small">
                  <Step status="finish" title="Payment" />
                  {
                    whichVersion !== 'without_problem' ?
                      <Step status="finish" title="Problems" /> : null
                  }
                  <Step status="finish" title="Select Type" />
                  <Step status="wait" title="Select Doctor" />
                  <Step status="wait" title="Select Slot" />
                  <Step status="wait" title="Book Appointment" />
                </Steps>

              </div>
            </div>
            <div className="col-sm-1 col-md-1 col-xl-1"></div>
          </div>
          <div className="row mt-3">
            <div className="col-12">
              <div className="row text-left">
                <div className="col-sm-1 col-md-1 col-lg-1 col-xl-1"></div>
                <div className="col-sm-10 col-md-10 col-lg-10 col-xl-10 pb-3">

                  <div className="pt-5">
                    <h3 className={style.lblPayment}>Creating Appointment for ?</h3>
                  </div>
                  <div className="row px-3 pb-5">
                    <div className="col-sm-12 col-md-6 col-lg-6 col-xl-4 pl-0 mt-2">

                      <div className={style.chooseApp2}>
                        <div className="card_static_appiment" style={{ width: "100%", justifyContent: "space-around" }}>
                          <div className={style.div_img}>
                            <img
                              src="Images/team.png"
                              alt=""
                              className={style.productImg1_appoiment}
                            />
                          </div>
                          <div className={style.div_label_center_radio}>
                            <Radio.Group onChange={checkFamilyMembar} name="FamilyMembar" value={selectAppointment} size="middle">
                              <Radio value="Own"><h4 className={style.h33}>Own</h4></Radio>
                              <Radio value="familyMember"><h4 className={style.h33}>Family Member</h4> </Radio>
                            </Radio.Group>
                          </div>
                        </div>
                      </div>
                      {/* <div className="d-flex justify-content-between px-3">
                        <div>
                          <p className={style.listfamilylbl}>{familyMemberState.userName} </p>
                        </div>
                        <div className="pt-2" onClick={() => editFamilyMember()} style={{ cursor: 'pointer' }}>
                          <EditOutlined />
                        </div>
                      </div> */}
                    </div>

                    {!isOwn && familyMemberState?.userName != "" ?
                      <div className="col-sm-12 col-md-6 col-lg-6 col-xl-4 mt-2">
                        <div className={style.chooseApp}>
                          {
                            familyMemberState ?
                              <div
                                onClick={() => cardClick("Today")}
                                className="card_static_appiment"
                              >
                                <div className={style.div_edit} onClick={() => editFamilyMember()}>
                                  <EditOutlined className={style.iconEdit} />
                                </div>

                                <div className={style.div_img}>
                                  <img
                                    src="Images/team.png"
                                    alt=""
                                    className={style.productImg1_appoiment}
                                  />
                                </div>
                                <div className={style.div_label_center}>
                                  <h3 className={style.h33} style={{ fontSize: 20 }}>{familyMemberState?.userName}</h3>
                                  <p className={style.pp}>{familyMemberState?.relationship} </p>
                                </div>
                              </div> : null
                          }


                          {
                            // familyMemberState?.ohipNumber == "" || familyMemberState?.ohipNumber == null ?
                            //   <>
                            //     {
                            //       familyMemberState ?
                            //         <div>
                            //           <div className="alert alert-danger" role="alert">
                            //             Card not available !
                            //           </div>
                            //           <button type="button" className="btn btn-primary" onClick={() => familyModelOpen()}>Add</button>
                            //         </div> : null
                            //     }

                            //     <AddFamilyMember
                            //       title="Family Member"
                            //       open={familyModel}
                            //       close={familyModelClose}
                            //       getFamilyMember={getFamilyMember}
                            //       editFamilyMember={familyMemberState}
                            //     />
                            //   </>
                            //   : null
                          }
                        </div>
                      </div>
                      : null}
                  </div>

                  <div className="pt-3">
                    <h3 className={style.lblPayment}>Appointment Type ?</h3>
                  </div>
                  <div className="row pt-2">
                    <div className="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                      <div className={style.chooseApp}>
                        <div
                          onClick={() => cardClick("fastService")}
                          className={`${selectedCard === "fastService"
                            ? "card_selected_appoiment"
                            : "card_static_appiment"
                            }`}
                        >
                          <div className={style.div_img}>
                            <img
                              src="Images/watch.png"
                              alt=""
                              className={style.productImg_appoiment}
                            />
                          </div>
                          <div className={style.div_label_center}>
                            <h3 className={style.h33}>Fastest Service</h3>
                            <p className={style.pp}>
                              Join in Virtual Waiting Room
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                      <div className={style.chooseApp}>
                        <div
                          onClick={() => cardClick("Today")}
                          className={`${selectedCard === "Today"
                            ? "card_selected_appoiment"
                            : "card_static_appiment"
                            }`}
                        >
                          <div className={style.div_img}>
                            <img
                              src="Images/schedule.png"
                              alt=""
                              className={style.productImg1_appoiment}
                            />
                          </div>
                          <div className={style.div_label_center}>
                            <h3 className={style.h33}>Schedule</h3>
                            <p className={style.pp}>Regular Appointment </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                      <div className={style.chooseApp}>
                        {/* {whichVersion != "with_problem" ? null : ( */}
                        <div>
                          <div
                            onClick={() => cardClick("callback")}
                            className={`${selectedCard === "callback"
                              ? "card_selected_appoiment"
                              : "card_static_appiment"
                              }`}
                          >
                            <div className={style.div_img}>
                              <img
                                src="Images/call-back.png"
                                alt=""
                                className={style.productImg1_appoiment}
                              />
                            </div>
                            <div className={style.div_label_center}>
                              <h3 className={style.h33}>Callback</h3>
                              <p className={style.pp}>Create Callback Request </p>
                            </div>
                          </div>
                        </div>
                        {/* )} */}
                      </div>
                    </div>
                  </div>

                </div>
                <div className="col-sm-1 col-md-1 col-lg-1 col-xl-1"></div>
              </div>
            </div>
          </div>
        </div>
        <Modal footer={null} width={1000} title="Family Member List" visible={isModalVisible} onOk={handleOkFamily} onCancel={handleCancelFamily}>
          {loader ? (
            <div className="spinner">
              <Spin tip="Loading..."></Spin>
            </div>
          ) : (
            // <Table dataSource={familyMember} columns={data} style={{ whiteSpace: 'pre' }} pagination={{ defaultPageSize: 4, showSizeChanger: true, pageSizeOptions: ['5'] }} />
            <div className="row">
              <div className="col-12">
                <Table dataSource={familyMember} columns={data} scroll={{ x: 1000 }} />
              </div>
            </div>
          )}
        </Modal>


      </div>
      {/* <Footer /> */}

      {/* {
        selectAppointment == "Own" ?
          <div className="footer">
            <Footer
              location="searchDoctor"
              pageName="appoiment-type"
              hide="true"

            />
          </div> :
          <>
            <div className="footer">
              {selectedCard == "callback" ? (
                <Footer
                  location="select-callback"
                  pageName="select-callback"
                  hide="true"
                  disbleFooter={familyMemberState?.ohipNumber == "" || familyMemberState?.ohipNumber == null ? false : true}
                />
              ) : (
                <Footer
                  location="searchDoctor"
                  pageName="appoiment-type"
                  hide="true"
                  disbleFooter={familyMemberState?.ohipNumber == "" || familyMemberState?.ohipNumber == null ? false : true}
                />
              )}
            </div>
          </>
      } */}


      {/* <h1>selectAppointment : {selectAppointment}</h1>
      <h1>selectedCard : {selectedCard}</h1> */}


      {
        selectAppointment == "Own" && selectedCard == "fastService" ?
          <div className="footer">
            <Footer
              location="searchDoctor"
              pageName="appoiment-type"
              hide="true"
              // disbleFooter={familyMemberState?.ohipNumber == "" || familyMemberState?.ohipNumber == "null" ? false : true}
              disbleFooter={true}
            />
          </div> :
          null
      }
      {
        selectAppointment == "Own" && selectedCard == "Today" ?
          <div className="footer">
            <Footer
              location="searchDoctor"
              pageName="appoiment-type"
              hide="true"
              // disbleFooter={familyMemberState?.ohipNumber == "" || familyMemberState?.ohipNumber == "null" ? false : true}
              disbleFooter={true}
            />
          </div> :
          null
      }
      {
        selectAppointment == "Own" && selectedCard == "callback" ?
          <div className="footer">
            <Footer
              location="select-callback"
              pageName="select-callback"
              hide="true"
            />
          </div> :
          null
      }
      {
        selectAppointment == "familyMember" && selectedCard == "Today" ?
          <div className="footer">
            <Footer
              location="searchDoctor"
              pageName="appoiment-type"
              hide="true"
              // disbleFooter={familyMemberState?.ohipNumber == "" || familyMemberState?.ohipNumber == null ? false : true}
              disbleFooter={true}
            />
          </div> :
          null
      }
      {
        selectAppointment == "familyMember" && selectedCard == "fastService" ?
          <div className="footer">
            <Footer
              location="searchDoctor"
              pageName="appoiment-type"
              hide="true"
              // disbleFooter={familyMemberState?.ohipNumber == "" || familyMemberState?.ohipNumber == null ? false : true}
              disbleFooter={true}
            />
          </div> :
          null
      }
      {
        selectAppointment == "familyMember" && selectedCard == "callback" ?
          <div className="footer">
            <Footer
              location="select-callback"
              pageName="select-callback"
              hide="true"
            />
          </div> :
          null
      }



    </div >
  );
};

export default AppoimentType;
